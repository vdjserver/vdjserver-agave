#!/bin/bash
 
#parameters
JSON='${json}'
OUTDIR='${outdir}'

# check that input files are in place
echo "files used in this job are the following: ${files}"

for file in ${files}; do
  if [ -f $file ]; then
    echo $file >> .agave.archive
  else
    echo "ERROR: file $file was not found in the local directory during execution."
    echo "Please report this error in the "Feedback" section of the site along with the project and job names"
    exit 1
  fi
done

# Unpack the bin.tgz file containing the binaries
tar -xvf bin.tgz
EXECUTABLE=./bin/vdj_pipe

# Fail if we can't execute the executable
if [[ ! -x $EXECUTABLE ]]; then
  echo "ERROR: could not find the executable on the system. Please report this error."
  exit 2
fi

# Build up an ARGS string for the program
# Start with mandatory arguments
ARGS=""
# Boolean handler for -named sort
if [ -n '$JSON' ]; then echo $JSON > vdjpipe_config.json;
                        ARGS="${ARGS} --config vdjpipe_config.json"; fi
if [ -n "${OUTDIR}" ]; then ARGS="${ARGS} -O ${OUTDIR}"
                       else ARGS="${ARGS} -O ./";           fi
#if [ ${TRIM_HARD_A} == 1 ]; then ARGS="${ARGS} -trimHardA "; fi

echo "Job started on $(date)" 
# Run the actual program
$EXECUTABLE ${ARGS}
echo "Job ended on $(date)"

# Now, delete the bin/ directory
if [[ -d ./bin ]]; then
  rm -rf bin
fi

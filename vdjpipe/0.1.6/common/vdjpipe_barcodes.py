#
# Helper script to generate barcode file names
#
# Author: Scott Christley
# Date: Sep 8, 2016
#

from __future__ import print_function
import json
import argparse
import sys
from Bio import SeqIO

parser = argparse.ArgumentParser(description='Generate barcode filenames.')
parser.add_argument('--barcodeFiles', type=str, nargs=2, help='Generate barcode filenames', metavar=('output', 'barcodeFile'))

args = parser.parse_args()
if (args):

    # Helper to generate barcode file name
    if (args.barcodeFiles):
        fasta_reader = SeqIO.parse(open(args.barcodeFiles[1], "r"), "fasta")
        for query_record in fasta_reader:
            print ("python ./process_metadata.py --group", query_record.id, "file", "process_metadata.json")
            print ("python ./process_metadata.py --entry output",
                   query_record.id, "vdjPipe processed_sequence",
                   args.barcodeFiles[0].replace('{MID}', query_record.id),
                   '"Final Post-Filter Sequences, Barcode (' + query_record.id + ')"',
                   "read", "process_metadata.json")

else:
    # invalid arguments
    parser.print_help()

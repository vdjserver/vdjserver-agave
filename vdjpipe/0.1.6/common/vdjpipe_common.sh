#
# VDJServer VDJPipe common functions
#
# This script relies upon global variables
# source vdjpipe_common.sh
#
# Author: Scott Christley
# Date: Sep 6, 2016
# 

# required global variables:
# VDJ_PIPE
# PYTHON
# BIO_PYTHON
# AGAVE_JOB_ID
# AGAVE_JOB_NAME
# AGAVE_LOG_NAME
# and...
# The agave app input and parameters

# ----------------------------------------------------------------------------
# Process workflow metadata
APP_NAME=vdjPipe

function initProcessMetadata() {
    $PYTHON ./process_metadata.py --init $APP_NAME $AGAVE_JOB_ID process_metadata.json
}

function addStatisticsFile() {
    $PYTHON ./process_metadata.py --entry statistics "$1" "$2" "$3" "$4" "$5" "$6" process_metadata.json
}

function addLogFile() {
    $PYTHON ./process_metadata.py --entry log "$1" "$2" "$3" "$4" "$5" "$6" process_metadata.json
}

function addConfigFile() {
    $PYTHON ./process_metadata.py --entry config "$1" "$2" "$3" "$4" "$5" "$6" process_metadata.json
}

function addOutputFile() {
    $PYTHON ./process_metadata.py --entry output "$1" "$2" "$3" "$4" "$5" "$6" process_metadata.json
}

function addGroup() {
    $PYTHON ./process_metadata.py --group "$1" "$2" process_metadata.json
}

function addCalculation() {
    $PYTHON ./process_metadata.py --calc $1 process_metadata.json
}

# ----------------------------------------------------------------------------

function noArchive() {
    echo $1 >> .agave.archive
}

function print_versions() {
    # Start
    echo "VERSIONS:"
    echo "  $($VDJ_PIPE --version 2>&1)"
    echo ""
}

function print_parameters() {
    echo "Input files:"
    echo "SequenceFASTQ=$SequenceFASTQ"
    echo "SequenceFASTA=$SequenceFASTA"
    echo "SequenceQualityFiles=$SequenceQualityFiles"
    echo "SequenceForwardPairedFiles=$SequenceForwardPairedFiles"
    echo "SequenceReversePairedFiles=$SequenceReversePairedFiles"
    echo "ForwardPrimerFile=$ForwardPrimerFile"
    echo "ReversePrimerFile=$ReversePrimerFile"
    echo "BarcodeFile=$BarcodeFile"
    echo ""
    echo "Application parameters:"
    echo "Workflow=$Workflow"
    echo "Merge paired-end reads:"
    echo "MergeMinimumScore=${MergeMinimumScore}"
    echo "MergeOutputFilename=${MergeOutputFilename}"
    echo "Pre-filter statistics:"
    echo "PreFilterStatisticsFlag=$PreFilterStatisticsFlag"
    echo "PreFilterStatisticsFilename=${PreFilterStatisticsFilename}"
    echo "Filters:"
    echo "FilterFlag=$FilterFlag"
    echo "MinimumAverageQuality=$MinimumAverageQuality"
    echo "MinimumLength=$MinimumLength"
    echo "MaximumHomopolymer=${MaximumHomopolymer}"
    echo "Post-filter statistics:"
    echo "PostFilterStatisticsFlag=$PostFilterStatisticsFlag"
    echo "PostFilterStatisticsFilename=${PostFilterStatisticsFilename}"
    echo "Barcodes:"
    echo "Barcode=$Barcode"
    echo "BarcodeLocation=${BarcodeLocation}"
    echo "BarcodeDiscard=${BarcodeDiscard}"
    echo "BarcodeGenerateHistogram=${BarcodeGenerateHistogram}"
    echo "BarcodeMaximumMismatches=${BarcodeMaximumMismatches}"
    echo "BarcodeTrim=${BarcodeTrim}"
    echo "BarcodeSearchWindow=${BarcodeSearchWindow}"
    echo "BarcodeSplitFlag=${BarcodeSplitFlag}"
    echo "Forward primer:"
    echo "ForwardPrimer=${ForwardPrimer}"
    echo "ForwardPrimerMaximumMismatches=${ForwardPrimerMaximumMismatches}"
    echo "ForwardPrimerTrim=${ForwardPrimerTrim}"
    echo "ForwardPrimerSearchWindow=${ForwardPrimerSearchWindow}"
    echo "Reverse primer:"
    echo "ReversePrimer=${ReversePrimer}"
    echo "ReversePrimerMaximumMismatches=${ReversePrimerMaximumMismatches}"
    echo "ReversePrimerTrim=${ReversePrimerTrim}"
    echo "ReversePrimerSearchWindow=${ReversePrimerSearchWindow}"
    echo "Find unique sequences:"
    echo "FindUniqueFlag=$FindUniqueFlag"
    echo "FindUniqueOutputFilename=${FindUniqueOutputFilePrefix}"
    echo "FindUniqueDuplicatesFilename=${FindUniqueOutputFilePrefix}"
    echo "Final output:"
    echo "FinalOutputFilename=$FinalOutputFilename"
    echo ""
}

function run_vdjpipe_workflow() {
    group="group1"
    initProcessMetadata
    addLogFile $APP_NAME log stdout "${AGAVE_LOG_NAME}.out" "Job Output Log" "log"
    addLogFile $APP_NAME log stderr "${AGAVE_LOG_NAME}.err" "Job Error Log" "log"
    addLogFile $APP_NAME log agave_log .agave.log "Agave Output Log" "log"
    addGroup $group file

    # Construct VDJPipe config file
    $PYTHON ./vdjpipe_create_config.py --init summary.txt vdjpipe_config.json
    addConfigFile $group config main "vdjpipe_config.json" "VDJPipe Input Configuration" "json"
    addLogFile $group log summary "summary.txt" "VDJPipe Output Summary" "log"

    # Paired-end read workflow needs to merge
    if [ "$Workflow" = "paired" ]; then
	if [ -z "$MergeMinimumScore" ]; then
	    MergeMinimumScore=10
	fi
	if [ -z "$MergeOutputFilename" ]; then
	    MergeOutputFilename="merged.fastq"
	fi
	# Construct VDJPipe config file
	$PYTHON ./vdjpipe_create_config.py --init merge_summary.txt vdjpipe_paired_config.json
	$PYTHON ./vdjpipe_create_config.py vdjpipe_paired_config.json --merge $MergeMinimumScore $MergeOutputFilename --forwardReads $SequenceForwardPairedFiles --reverseReads $SequenceReversePairedFiles
	addConfigFile $group config paired "vdjpipe_paired_config.json" "VDJPipe Read Merging Configuration" "json"
	addLogFile $group log merge_summary "merge_summary.txt" "VDJPipe Read Merging Output Summary" "log"

        noArchive $SequenceForwardPairedFiles
	noArchive $SequenceReversePairedFiles

        # run the paired merging
	echo "Merging paired-end reads."
	$VDJ_PIPE --config vdjpipe_paired_config.json
	addCalculation merge_paired_reads

	# Add merged output
	if [ -z "$MergeOutputFilename" ]; then
	    MergeOutputFilename="merged.fastq"
	fi
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --fastq $MergeOutputFilename
	addOutputFile $group $APP_NAME merged_sequence "$MergeOutputFilename" "Merged Pre-Filter Sequences" "read"
    fi

    # Main workflow
    if [ -n "$SequenceFASTQ" ]; then
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --fastq $SequenceFASTQ
	noArchive $SequenceFASTQ
    fi
    if [ -n "$SequenceFASTA" ]; then
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --fasta $SequenceFASTA --quals $SequenceQualityFiles
	noArchive $SequenceFASTA
        noArchive $SequenceQualityFiles
    fi

    # Pre-filter statistics
    if [[ $PreFilterStatisticsFlag -eq 1 ]]; then
	if [ -z "$PreFilterStatisticsFilename" ]; then
	    PreFilterStatisticsFilename="pre-filter_"
	fi
	if [[ $PostFilterStatisticsFlag -eq 1 ]]; then
	    addStatisticsFile $group pre composition "pre-filter_composition.csv" "Nucleotide Composition" "tsv"
	    addStatisticsFile $group pre gc_hist "pre-filter_gc_hist.csv" "GC% Histogram" "tsv"
	    addStatisticsFile $group pre heat_map "pre-filter_heat_map.csv" "Heatmap" "tsv"
	    addStatisticsFile $group pre len_hist "pre-filter_len_hist.csv" "Sequence Length Histogram" "tsv"
	    addStatisticsFile $group pre mean_q_hist "pre-filter_mean_q_hist.csv" "Mean Quality Histogram" "tsv"
	    addStatisticsFile $group pre qstats "pre-filter_qstats.csv" "Quality Scores" "tsv"
	    addCalculation "pre-filter_statistics"
	else
	    # if no post then must be just a single statistics run
	    PreFilterStatisticsFilename="stats_"
	    addStatisticsFile $group stats composition "stats_composition.csv" "Nucleotide Composition" "tsv"
	    addStatisticsFile $group stats gc_hist "stats_gc_hist.csv" "GC% Histogram" "tsv"
	    addStatisticsFile $group stats heat_map "stats_heat_map.csv" "Heatmap" "tsv"
	    addStatisticsFile $group stats len_hist "stats_len_hist.csv" "Sequence Length Histogram" "tsv"
	    addStatisticsFile $group stats mean_q_hist "stats_mean_q_hist.csv" "Mean Quality Histogram" "tsv"
	    addStatisticsFile $group stats qstats "stats_qstats.csv" "Quality Scores" "tsv"
	    addCalculation "statistics"
	fi
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --statistics $PreFilterStatisticsFilename
    fi

    # Filtering
    if [[ $FilterFlag -eq 1 ]]; then
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --length $MinimumLength
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --quality $MinimumAverageQuality
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --homopolymer $MaximumHomopolymer
	addCalculation length_filtering
	addCalculation quality_filtering
	addCalculation homopolymer_filtering
    fi

    # Post-filter statistics
    if [[ $PostFilterStatisticsFlag -eq 1 ]]; then
	if [ -z "$PostFilterStatisticsFilename" ]; then
	    PostFilterStatisticsFilename="post-filter_"
	fi
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --statistics $PostFilterStatisticsFilename
	addStatisticsFile $group post composition "post-filter_composition.csv" "Nucleotide Composition" "tsv"
	addStatisticsFile $group post gc_hist "post-filter_gc_hist.csv" "GC% Histogram" "tsv"
	addStatisticsFile $group post heat_map "post-filter_heat_map.csv" "Heatmap" "tsv"
	addStatisticsFile $group post len_hist "post-filter_len_hist.csv" "Sequence Length Histogram" "tsv"
	addStatisticsFile $group post mean_q_hist "post-filter_mean_q_hist.csv" "Mean Quality Histogram" "tsv"
	addStatisticsFile $group post qstats "post-filter_qstats.csv" "Quality Scores" "tsv"
	addCalculation "post-filter_statistics"
    fi

    # Barcode
    if [[ $Barcode -eq 1 ]]; then
	ARGS=""
	if [[ -n "$BarcodeLocation" ]]; then
	    ARGS="${ARGS} $BarcodeLocation"
	else
	    ARGS="${ARGS} forward"
	fi
	if [[ $BarcodeDiscard -eq 1 ]]; then
	    ARGS="${ARGS} $BarcodeDiscard"
	else
	    ARGS="${ARGS} False"
	fi
	if [[ -n "$BarcodeMaximumMismatches" ]]; then
	    ARGS="${ARGS} $BarcodeMaximumMismatches"
	else
	    ARGS="${ARGS} 0"
	fi
	ARGS="${ARGS} $BarcodeFile"
	if [[ $BarcodeTrim -eq 1 ]]; then
	    ARGS="${ARGS} $BarcodeTrim"
	else
	    ARGS="${ARGS} False"
	fi
	if [[ -n "$BarcodeSearchWindow" ]]; then
	    ARGS="${ARGS} $BarcodeSearchWindow"
	else
	    ARGS="${ARGS} 30"
	fi
	ARGS="${ARGS} MID"

	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --barcode $ARGS
	addCalculation barcode_demultiplexing
	noArchive $BarcodeFile
    fi

    # Forward primer
    if [[ $ForwardPrimer -eq 1 ]]; then
	ARGS=""
	if [[ -n "$ForwardPrimerMaximumMismatches" ]]; then
	    ARGS="${ARGS} $ForwardPrimerMaximumMismatches"
	else
	    ARGS="${ARGS} 0"
	fi
	ARGS="${ARGS} $ForwardPrimerFile"
	if [[ $ForwardPrimerTrim -eq 1 ]]; then
	    ARGS="${ARGS} $ForwardPrimerTrim"
	else
	    ARGS="${ARGS} False"
	fi
	if [[ -n "$ForwardPrimerSearchWindow" ]]; then
	    ARGS="${ARGS} $ForwardPrimerSearchWindow"
	else
	    ARGS="${ARGS} 30"
	fi

	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --forwardPrimer $ARGS
	addCalculation forward_primer
	noArchive $ForwardPrimerFile
    fi

    # Reverse primer
    if [[ $ReversePrimer -eq 1 ]]; then
	ARGS=""
	if [[ -n "$ReversePrimerMaximumMismatches" ]]; then
	    ARGS="${ARGS} $ReversePrimerMaximumMismatches"
	else
	    ARGS="${ARGS} 0"
	fi
	ARGS="${ARGS} $ReversePrimerFile"
	if [[ $ReversePrimerTrim -eq 1 ]]; then
	    ARGS="${ARGS} $ReversePrimerTrim"
	else
	    ARGS="${ARGS} False"
	fi
	if [[ -n "$ReversePrimerSearchWindow" ]]; then
	    ARGS="${ARGS} $ReversePrimerSearchWindow"
	else
	    ARGS="${ARGS} 30"
	fi

	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --reversePrimer $ARGS
	addCalculation reverse_primer
	noArchive $ReversePrimerFile
    fi

    # Find unique sequences
    if [[ $FindUniqueFlag -eq 1 ]]; then
	if [ -z "$FindUniqueOutputFilename" ]; then
	    FindUniqueOutputFilename="unique.fasta"
	fi
	if [ -z "$FindUniqueDuplicatesFilename" ]; then
	    FindUniqueDuplicatesFilename="unique-duplicates.tsv"
	fi
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --unique $FindUniqueOutputFilename $FindUniqueDuplicatesFilename
	addOutputFile $group $APP_NAME sequence "$FindUniqueOutputFilename" "Unique Post-Filter Sequences" "read"
	addOutputFile $group $APP_NAME duplicates "$FindUniqueDuplicatesFilename" "Unique Sequence Duplicates Table" "tsv"
	addCalculation find_unique_sequences
    fi

    # Write final sequences
    if [[ -n "$FinalOutputFilename" ]]; then
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --write $FinalOutputFilename
	if [[ $BarcodeSplitFlag -eq 1 ]]; then
	    $BIO_PYTHON ./vdjpipe_barcodes.py --barcodeFiles $FinalOutputFilename $BarcodeFile >vdjpipe_barcodes.sh
	    bash ./vdjpipe_barcodes.sh
	    rm -f vdjpipe_barcodes.sh
	else
	    addOutputFile $group $APP_NAME processed_sequence "$FinalOutputFilename" "Total Post-Filter Sequences" "read"
	fi
    fi

    # Barcode histogram
    if [[ $BarcodeGenerateHistogram -eq 1 ]]; then
	$PYTHON ./vdjpipe_create_config.py vdjpipe_config.json --barcodeHistogram MID
	addStatisticsFile $group barcode value "MID.tsv" "Barcode Histogram" "tsv"
	addStatisticsFile $group barcode score "MID-score.tsv" "Barcode Score Histogram" "tsv"
	addCalculation barcode_histogram
    fi

    # run the main workflow
    echo "Main VDJPipe workflow"
    $VDJ_PIPE --config vdjpipe_config.json
}

#
# VDJServer VDJPipe Agave wrapper script
# for small (vdj-exec-02) execution system
# 
# Author: Scott Christley
# Date: Sep 6, 2016
#

# These get set by Agave

# input files
SequenceFASTQ="${SequenceFASTQ}"
SequenceFASTA="${SequenceFASTA}"
SequenceQualityFiles="${SequenceQualityFiles}"
SequenceForwardPairedFiles="${SequenceForwardPairedFiles}"
SequenceReversePairedFiles="${SequenceReversePairedFiles}"
ForwardPrimerFile="${ForwardPrimerFile}"
ReversePrimerFile="${ReversePrimerFile}"
BarcodeFile="${BarcodeFile}"

# application parameters
Workflow=${Workflow}
# File merging
MergeMinimumScore=${MergeMinimumScore}
MergeOutputFilename=${MergeOutputFilename}
# pre-filter statistics
PreFilterStatisticsFlag=${PreFilterStatisticsFlag}
PreFilterStatisticsFilename=${PreFilterStatisticsFilename}
# FilterSeq
FilterFlag=${FilterFlag}
MinimumAverageQuality=${MinimumAverageQuality}
MinimumLength=${MinimumLength}
MaximumHomopolymer=${MaximumHomopolymer}
# post-filter statistics
PostFilterStatisticsFlag=${PostFilterStatisticsFlag}
PostFilterStatisticsFilename=${PostFilterStatisticsFilename}
# Barcodes
Barcode=${Barcode}
BarcodeLocation=${BarcodeLocation}
BarcodeDiscard=${BarcodeDiscard}
BarcodeGenerateHistogram=${BarcodeGenerateHistogram}
BarcodeMaximumMismatches=${BarcodeMaximumMismatches}
BarcodeTrim=${BarcodeTrim}
BarcodeSearchWindow=${BarcodeSearchWindow}
BarcodeSplitFlag=${BarcodeSplitFlag}
# Forward primer
ForwardPrimer=${ForwardPrimer}
ForwardPrimerMaximumMismatches=${ForwardPrimerMaximumMismatches}
ForwardPrimerTrim=${ForwardPrimerTrim}
ForwardPrimerSearchWindow=${ForwardPrimerSearchWindow}
# Reverse primer
ReversePrimer=${ReversePrimer}
ReversePrimerMaximumMismatches=${ReversePrimerMaximumMismatches}
ReversePrimerTrim=${ReversePrimerTrim}
ReversePrimerSearchWindow=${ReversePrimerSearchWindow}
# Find unique sequences
FindUniqueFlag=${FindUniqueFlag}
FindUniqueOutputFilename="${FindUniqueOutputFilename}"
FindUniqueDuplicatesFilename="${FindUniqueDuplicatesFilename}"
#
FinalOutputFilename="${FinalOutputFilename}"

# use docker
VDJ_PIPE_DOCKER_IMAGE="vdjserver/vdj_pipe:v0.1.6"
VDJ_PIPE="docker run -v $PWD:/data -w /data $VDJ_PIPE_DOCKER_IMAGE /vdjpipe-root/vdjpipe/out/apps/gcc-4.9.2/release/link-static/threading-multi/vdj_pipe"
# use local python for speed
PYTHON="python"
# use docker python as it has biopython
BIO_PYTHON="docker run -v $PWD:/data -w /data $VDJ_PIPE_DOCKER_IMAGE python"

# Agave info
AGAVE_JOB_ID=${AGAVE_JOB_ID}
AGAVE_JOB_NAME="${AGAVE_JOB_NAME}"
AGAVE_LOG_NAME="${AGAVE_JOB_NAME}"

# bring in common functions
source vdjpipe_common.sh

# Start
printf "START at $(date)\n\n"

print_parameters
print_versions
run_vdjpipe_workflow

# End
printf "DONE at $(date)\n\n"

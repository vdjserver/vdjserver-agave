#
SYSTEM=stampede
VER=0.1.6

# Copy the vdj_pipe executable to a bin directory
# and create a bin.tgz
#
# For example:
# mkdir bin
# cp $WORK/stampede/vdj_pipe/v0.1.6/out/bin/vdj_pipe bin
# tar zcvf bin.tgz bin

# delete old working area in agave
files-delete /apps/vdj_pipe/$VER/$SYSTEM

# upload files
files-mkdir -N $SYSTEM /apps/vdj_pipe/$VER
files-mkdir -N test /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F bin.tgz /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F ../common/vdjpipe_common.sh /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F ../common/vdjpipe_create_config.py /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F ../common/vdjpipe_barcodes.py /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F ../../../common/process_metadata.py /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F vdj_pipe.sh /apps/vdj_pipe/$VER/$SYSTEM
files-upload -F vdj_pipe.json /apps/vdj_pipe/$VER/$SYSTEM

files-upload -F test/test.sh /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F test/test-cli.json /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F test/test-paired.json /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F test/test-mixed.json /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_2000.fastq /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/Sample00013.fna /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/Sample00013.qual /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/test_r1.fastq /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/test_r2.fastq /apps/vdj_pipe/$VER/$SYSTEM/test

files-upload -F test/test-roche.json /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR765688.fastq /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR765688_MIDs.fasta /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/SRX190717_CPrimers.fasta /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/SRX190717_CPrimers_rev.fasta /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/SRX190717_VPrimers.fasta /apps/vdj_pipe/$VER/$SYSTEM/test

files-upload -F test/test-grieff.json /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/ERR346600_1.fastq /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/ERR346600_2.fastq /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/Greiff2014_VPrimers.fasta /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/Greiff2014_CPrimers.fasta /apps/vdj_pipe/$VER/$SYSTEM/test
files-upload -F ../common/test/Greiff2014_CPrimers_rev.fasta /apps/vdj_pipe/$VER/$SYSTEM/test

files-list -L /apps/vdj_pipe/$VER/$SYSTEM
if [ "$(files-list -L /apps/vdj_pipe/$VER/$SYSTEM | wc -l)" -ne 9 ]; then
    echo "ERROR: Wrong number of file entries in app directory."
fi

files-list -L /apps/vdj_pipe/$VER/$SYSTEM/test

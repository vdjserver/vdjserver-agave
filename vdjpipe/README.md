# Agave VDJPipe Application for VDJServer

The general naming of the Agave VDJPipe application is
`vdj_pipe-SYSTEM-VER` where `SYSTEM` is the short name of the
execution system and `VER` is the version number of VDJPipe. We
haven't moved to using just the `MAJOR.MINOR` version numbers for the
Agave application yet, but we should with the next major release of
VDJPipe.

* vdj_pipe-ls5-0.1.7: VDJPipe 0.1.7 for Lonestar 5.

* vdj_pipe-small-0.1.7: VDJPipe 0.1.7 for vdj-exec-02.

* vdj_pipe-stampede2-0.1.7: VDJPipe 0.1.7 for Stampede2.

VDJPipe is one of the apps which can run small jobs on vdj-exec-02.

Note that almost all of the binaries for the application are bundled
together into the Agave application, making each application
self-contained with minimal external dependencies. This also acts as
an effective archival and versioning mechanism. There are exceptions
however as some applications assets are quite large causing
inefficiencies if bundled with the main application. For VDJPipe, the
exception is for the small execution system (vdj-exec-02). That system
uses a docker image of VDJPipe so no binaries are copied over, instead
the docker image name and version are specified.

## Directory structure and files

Directories are split by VDJPipe version. Under each are a set of
directories for the different execution systems:

* common: scripts and data common for all execution systems. Most of
  the main processing workflow is written to be system independent and
  resides in `vdjpipe_common.sh`. Data for tests are stored here under
  the `test` directory.

* ls5: VDJPipe for ls5.tacc.utexas.edu

* small: VDJPipe for vdj-exec-02 using Docker.

* stampede2: VDJPipe for stampede2.tacc.utexas.edu.

Each execution system directory has a set of files that is similar
contains customizations specific to the execution system. The
customizations handle difference such as machine/queue names, system
modules, directory paths, environment variables, parallel processing
settings, etc.

* upload-app.sh: Simple shell script to use Agave CLI command to
  upload all of the files for the application into the Agave storage
  system. Typically defining `SYSTEM` and `VER` at the top of the
  script is sufficient.

* vdj_pipe.sh: The job submission template for the Agave
  application. When Agave runs a job, it takes this template script
  and modifies it into a submission script that is then submitted to
  the queue on the execution system. This script has been written to
  move as much system independent code into the common script, which
  are called as bash functions. The script setups the execution
  environment by extracting binaries, setting system modules and
  environment variables.

* vdj_pipe.json: The JSON definition for the Agave VDJPipe
  application. This definition should be identical for each execution
  system except for a few parameters. The `inputs` and `parameters`
  should be identical for all execution systems.

* test: A set of JSON job scripts for manually testing the Agave
  application. Archive is off by default, so that the scratch
  directory can be reviewed and debugged. All jobs should run
  successfully and the job error file in the scratch directory should
  be checked to insure no errors occurred. All job data is stored in
  the `common` directory but is uploaded separately into the specific
  application's directory tree within Agave.

* bundle: This is a temporary work area for combining all the binaries
  together into a `binaries.tgz` file that is upload as part of the
  application. This directory is excluded from the git repository.

## Deploying and Publishing Agave VDJPipe application

If you have a new VDJPipe version, or a new execution system, then you
will be starting from scratch. Generally, you should copy all the
files from another execution system as a starting point, then make the
necessary modifications to them. The complete steps include:

1. Logon execution system as vdj account.

2. Compiling VDJPipe

3. Modify vdj_pipe.json

4. Modify vdj_pipe.sh

5. Modify test scripts

6. Create binary bundle

7. Upload files to Agave

8. Create Agave application

9. Submit test jobs

10. Publish application

If you are modifying an existing version versus starting from scratch,
then generally steps 6, 7 and 9, are all that is performed. However,
if the JSON definition was modified, then step 8 needs to be performed
to update the Agave app definition.

### Logon execution system as vdj account

You want to be on the same execution system for the application you
are deploying. You cannot login directly into the vdj account, instead
you need to login with your user account, then `su` to the vdj account.

```
$ ssh schristl@stampede2.tacc.utexas.edu
Password: 
TACC Token Code:

$ su - vdj
Password: 

$ source vdjserver.env
$ auth-tokens-refresh
```

Use the vdj account password for the `su` command. The `source`
command sets up some environment like adding the Agave CLI tools to
the path. If `auth-tokens-refresh` returns an error, then the token
may have expired, so use `auth-tokens-create -u vdj` to create a new
one. You will need the Agave password for the vdj account. If you need
to specify the API client key and secret, be sure to use either the
`vdj_dev` or `vdj_staging` client so that your tokens do not conflict
with the VDJServer production system using the `vdj` client.

You can verify that the Agave CLI tools are working by trying a few commands:

```
$ files-list /
$ jobs-list -l 10
```

### Compiling VDJPipe

The `vdj-exec-02` execution system uses Docker so no compiling is
performed, instead new images are pulled from docker hub. For `ls5`
and `stampede2`, compilation is done in `$WORK/vdj_pipe`. These are
primarily notes to help document and may not be a complete set of
instructions. On a new system, you may need to figure out the proper
way to compile.

#### VDJPipe 0.1.7 on Stampede2

VDJPipe requires boost and uses the boost build system. This involves
copying two `jam` files, one to your home directory and one to the
directory right above the VDJPipe source code. To build, I `git clone`
the VDJPipe repository then checkout the appropriate version tag.

The main readme for VDJPipe says to look at doc/build.txt for
instructions. Those instructions are mostly accurate but if something
is not clear, then also look at the Dockerfile which has instructions
for building as well. The main steps involve building boost, setting
up the jam files for your compiler (GCC) and to point to boost source
tree, then compiling VDJPipe.

*USE ONLY BOOST VERSION 1.57.0*

```
$ cd $WORK/boost
$ tar zxvf boost_1_57_0.tar.gz
# build boost...

$ cd $WORK/vdj_pipe
$ git clone https://schristley@bitbucket.org/vdjserver/vdj_pipe.git v0.1.7
$ cd v0.1.7
$ git checkout v0.1.7
$ cp doc/boost-build.jam ..
$ cp doc/user-config.jam ~
# Edit boost-build.jam
# Edit user-config.jam

$ b2 release
```

On Stampede2, the build gets an error when trying to link the
executable, but it is easy to miss the error message. However, if you
look under the `out/apps` directory tree, then you won't find the
vdj_pipe executable. If you run `b2 release` again, you will see the
error message directly. The problem is that `libbz2` is trying to be
linked as static, but due to security issues that library is now only
available as shared. We've not been able to figure out how to tell
boost to use shared so I just manually run the command.

Here is the output with the error message:

```
$ b2 release
Performing configuration checks

    - zlib                     : yes (cached)
    - zlib                     : yes (cached)
...patience...
...patience...
...patience...
...patience...
...found 6338 targets...
...updating 1 target...
gcc.link /work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe
/opt/apps/gcc/5.4.0/bin/ld: cannot find -lbz2
collect2: error: ld returned 1 exit status

    "g++"    -o "/work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe" -Wl,--start-group "/work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe.o" "/work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/bin/gcc-5.4.0/release/link-static/threading-multi/libvdj_pipe.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/program_options/build/gcc-5.4.0/release/link-static/threading-multi/libboost_program_options.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/iostreams/build/gcc-5.4.0/release/link-static/threading-multi/libboost_iostreams.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/filesystem/build/gcc-5.4.0/release/link-static/threading-multi/libboost_filesystem.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/chrono/build/gcc-5.4.0/release/link-static/threading-multi/libboost_chrono.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/system/build/gcc-5.4.0/release/link-static/threading-multi/libboost_system.a"  -Wl,-Bstatic -lbz2 -Wl,-Bdynamic -lz -lrt -Wl,--end-group -pthread 

...failed gcc.link /work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe...
...failed updating 1 target...
```

Cut/paste the `g++` line and change `-Wl,-Bstatic -lbz2` to
`-Wl,-Bdynamic -lbz2` then run the command. For the above example, it
looks like this:

```
$     "g++"    -o "/work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe" -Wl,--start-group "/work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe.o" "/work/01114/vdj/stampede2/vdj_pipe/v0.1.7/out/bin/gcc-5.4.0/release/link-static/threading-multi/libvdj_pipe.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/program_options/build/gcc-5.4.0/release/link-static/threading-multi/libboost_program_options.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/iostreams/build/gcc-5.4.0/release/link-static/threading-multi/libboost_iostreams.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/filesystem/build/gcc-5.4.0/release/link-static/threading-multi/libboost_filesystem.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/chrono/build/gcc-5.4.0/release/link-static/threading-multi/libboost_chrono.a" "/work/01114/vdj/stampede2/boost/boost_1_57_0/bin.v2/libs/system/build/gcc-5.4.0/release/link-static/threading-multi/libboost_system.a"  -Wl,-Bdynamic -lbz2 -Wl,-Bdynamic -lz -lrt -Wl,--end-group -pthread 
```

Now there is a vdj_pipe executable under `out/apps`, and for this
above build, the exact file is:

```
$ ls -l out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe
-rwx------ 1 vdj G-803419 3484968 Jan 26 18:26 out/apps/gcc-5.4.0/release/link-static/threading-multi/vdj_pipe
```

This executable file can be copied to the application bundle.

#### VDJPipe 0.1.7 on vdj-exec-02

If the VDJPipe repository was properly tagged, then an automatic build
of the Docker image will executed and made available on [Docker
hub](https://hub.docker.com/r/vdjserver/vdj_pipe/). SSH to vdj-exec-02
as the vdj account, and use `docker pull` to pull the image. Verify
that you can run the image. That's it!

### Modify vdj_pipe.json

Customize for the execution system:

* name: vdj_pipe-SYSTEM where `SYSTEM` is the short name for the
  execution system (ls5, stampede2, small).

* version: version of VDJPipe. This will be added to the
  end of `name` to create the unique Agave app id.

* shortDescription, longDescription: basic description

* deploymentPath: `/apps/vdj_pipe/VER/SYSTEM` where `VER` and `SYSTEM`
  are the values used in all the other places,
  e.g. `/apps/vdj_pipe/0.1.7/stampede2`.

* executionSystem: Agave id for execution system,
  e.g. `stampede2.tacc.utexas.edu`

The execution system definition defines much of the technical details
of the system like the scheduling system, queue names, and so on but
some of these defaults can be overridden for specifications
applications. You should review these if they are defined:
`defaultQueue`, `defaultNodeCount`, `defaultProcessorsPerNode`,
`defaultRequestedTime`. There may be different attributes for
different types of execution systems.

### Modify vdj_pipe.sh

It's important that this script defines environment variables for the
`inputs` and `parameters` defined in the app JSON because this is the
only way to get those values. Agave uses this file as a template, and
search/replace those variables in the script with their values, as it
write the job submission script.

This script should load appropriate system modules, unarchive the
binary bundle, setup environment variables, and then call the main
system independent functions to run the workflow. Programs and the
commands to execute them may vary from system to system, so most apps
define environment variable with that information.

For the execution systems using Docker, the environment variable
`VDJ_PIPE_DOCKER_IMAGE` defines the docker image name and
tag. Actually running programs thus involves running that docker image
with the appropriate parameters.

### Modify test scripts

Each test script needs `appId` and `executionSystem` to be
defined. Also all directory paths in the `inputs` and `parameters`
need to point to the proper `VER` and `SYSTEM`.

### Create binary bundle

The Agave VDJPipe app is simply the vdj_pipe executable put into a
`bin` directory then archived into `bin.tgz`. The `upload-app.sh`
script contains notes for the commands to be used in comments at the
beginning of the script. These notes are approximate as I've not tried
to setup a complete automated system.

### Upload files to Agave

All the files for the application bundle need to be uploaded before
the Agave application can be defined. The `upload-bundle.sh` should be
modified with the appropriate `SYSTEM` and `VER` values, as
adding/removing files to be uploaded. The scripts uses the Agave CLI
so you need an active token for the vdj account.

This script also contains notes for the commands used to create the
binary bundle.

### Create Agave application

The application can be created and updated with the same command.

```
$ apps-addupdate -F vdj_pipe.json
Successfully added app vdj_pipe-stampede2-0.1.7
```

Once the application is defined in Agave, the `apps-addupdate` command
only needs to be re-run when the JSON definition changes. For most
changes, you can just re-upload the binary bundle then run a test job.

### Submit test jobs

All tests should submit and run successfully. Verify that they
actually ran the application you expected. Review the log files in the
scratch directory for any errors. If you find errors, you will need to
go back and repeat appropriate steps. It is also useful for debugging
to modify the submission script in the scratch directory, then use
`idev` to interactively run and debug the app.

```
$ jobs-submit -F test-cli.json
```

### Publish application

The application resides as a development application. That is, when a
job is run, Agave copies the `deploymentPath` directory tree to the
scratch directory. Thus any changes to files in that `deploymentPath`
will change the application. For production, we want a fixed version
of the application that won't change. The `apps-publish` command does
this by making a zip archive of `deploymentPath` and putting it in the
`publicAppsDir` for the storage system, which is `/api/v2/apps` for
VDJServer. In the process it prefixes a revision number to create a
unique Agave app id.

```
$ apps-publish vdj_pipe-stampede2-0.1.7
Successfully published app vdj_pipe-stampede2-0.1.7u1
```

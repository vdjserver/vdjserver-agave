# Agave RepCalc Application for VDJServer

The general naming of the Agave RepCalc application is
`repcalc-SYSTEM-VER` where `SYSTEM` is the short name of the
execution system and `VER` is the version number of RepCalc. We
use just the `MAJOR.MINOR` version numbers for the Agave
application. This allows for incorporating bug fix releases without
having to generate a new version.

* repcalc-ls5-1.0: RepCalc 1.0.x for Lonestar 5.

* repcalc-stampede2-1.0: RepCalc 1.0.x for Stampede2.

Note that almost all of the binaries for the application are bundled
together into the Agave application, making each application
self-contained with minimal external dependencies. This also acts as
an effective archival and versioning mechanism. There are exceptions
however as some applications assets are quite large causing
inefficiencies if bundled with the main application. For RepCalc, the
exception is for the R packages. The Alakhazam and Shazam package
depend upon a number of other packages that are not already installed
on either Lonestar5 or Stampede2. These packages are installed under
`$T_WORK/production/R_libs` instead of being bundled.

## Directory structure and files

Directories are split by RepCalc version. Under each are a set of
directories for the different execution systems:

* common: scripts and data common for all execution systems. Most of
  the main processing workflow is written to be system independent and
  resides in `repcalc_common.sh`. Data for tests are stored here under
  the `test` directory.

* ls5: RepCalc for ls5.tacc.utexas.edu

* stampede2: RepCalc for stampede2.tacc.utexas.edu.

Each execution system directory has a set of files that is similar
contains customizations specific to the execution system. The
customizations handle difference such as machine/queue names, system
modules, directory paths, environment variables, parallel processing
settings, etc.

* upload-bundle.sh: Simple shell script to use Agave CLI command to
  upload all of the files for the application into the Agave storage
  system. Typically defining `SYSTEM` and `VER` at the top of the
  script is sufficient.

* repcalc.sh: The job submission template for the Agave
  application. When Agave runs a job, it takes this template script
  and modifies it into a submission script that is then submitted to
  the queue on the execution system. This script has been written to
  move as much system independent code into the common script, which
  are called as bash functions. The script setups the execution
  environment by extracting binaries, setting system modules and
  environment variables.

* repcalc.json: The JSON definition for the Agave RepCalc
  application. This definition should be identical for each execution
  system except for a few parameters. The `inputs` and `parameters`
  should be identical for all execution systems.

* test: A set of JSON job scripts for manually testing the Agave
  application. Archive is off by default, so that the scratch
  directory can be reviewed and debugged. All jobs should run
  successfully and the job error file in the scratch directory should
  be checked to insure no errors occurred. All job data is stored in
  the `common` directory but is uploaded separately into the specific
  application's directory tree within Agave.

* bundle: This is a temporary work area for combining all the binaries
  together into a `binaries.tgz` file that is upload as part of the
  application. This directory is excluded from the git repository.

## Deploying and Publishing Agave RepCalc application

If you have a new RepCalc version, or a new execution system, then you
will be starting from scratch. Generally, you should copy all the
files from another execution system as a starting point, then make the
necessary modifications to them. The complete steps include:

1. Logon execution system as vdj account.

2. Install RepCalc

3. Modify repcalc.json

4. Modify repcalc.sh

5. Modify test scripts

6. Create binary bundle

7. Upload files to Agave

8. Create Agave application

9. Submit test jobs

10. Publish application

If you are modifying an existing version versus starting from scratch,
then generally steps 6, 7 and 9, are all that is performed. However,
if the JSON definition was modified, then step 8 needs to be performed
to update the Agave app definition.

### Logon execution system as vdj account

You want to be on the same execution system for the application you
are deploying. You cannot login directly into the vdj account, instead
you need to login with your user account, then `su` to the vdj account.

```
$ ssh schristl@stampede2.tacc.utexas.edu
Password: 
TACC Token Code:

$ su - vdj
Password: 

$ source vdjserver.env
$ auth-tokens-refresh
```

Use the vdj account password for the `su` command. The `source`
command sets up some environment like adding the Agave CLI tools to
the path. If `auth-tokens-refresh` returns an error, then the token
may have expired, so use `auth-tokens-create -u vdj` to create a new
one. You will need the Agave password for the vdj account. If you need
to specify the API client key and secret, be sure to use either the
`vdj_dev` or `vdj_staging` client so that your tokens do not conflict
with the VDJServer production system using the `vdj` client.

You can verify that the Agave CLI tools are working by trying a few commands:

```
$ files-list /
$ jobs-list -l 10
```

### Install RepCalc

The RepCalc Agave application is a combination of tools. This includes
VDJServer's repertoire-summarization repository, the Immcantation
python tools and the Immcantation R tools. Each of these tools need to
be installed locally in the `bundle` directory. The `upload-bundle.sh`
has basic instructions for doing the local installation.

### Modify repcalc.json

Customize for the execution system:

* name: repcalc-SYSTEM where `SYSTEM` is the short name for the
  execution system (ls5, stampede2).

* version: version of RepCalc. This will be added to the
  end of `name` to create the unique Agave app id.

* shortDescription, longDescription: basic description

* deploymentPath: `/apps/repcalc/VER/SYSTEM` where `VER` and `SYSTEM`
  are the values used in all the other places,
  e.g. `/apps/repcalc/1.0/stampede2`.

* executionSystem: Agave id for execution system,
  e.g. `stampede2.tacc.utexas.edu`

The execution system definition defines much of the technical details
of the system like the scheduling system, queue names, and so on but
some of these defaults can be overridden for specifications
applications. You should review these if they are defined:
`defaultQueue`, `defaultNodeCount`, `defaultProcessorsPerNode`,
`defaultRequestedTime`. There may be different attributes for
different types of execution systems.

### Modify repcalc.sh

It's important that this script defines environment variables for the
`inputs` and `parameters` defined in the app JSON because this is the
only way to get those values. Agave uses this file as a template, and
search/replace those variables in the script with their values, as it
write the job submission script.

This script should load appropriate system modules, unarchive the
binary bundle, setup environment variables, and then call the main
system independent functions to run the workflow. Programs and the
commands to execute them may vary from system to system, so most apps
define environment variable with that information.

### Modify test scripts

Each test script needs `appId` and `executionSystem` to be
defined. Also all directory paths in the `inputs` and `parameters`
need to point to the proper `VER` and `SYSTEM`.

### Create binary bundle

The Agave RepCalc app is a combination of multiple tools that will get
installed into local `bin` and `lib` directories within
`bundle`, and then archived into `binaries.tgz`. The
`upload-bundle.sh` script contains notes for the commands to be used
in comments at the beginning of the script. These notes are
approximate as I've not tried to setup a complete automated system.

### Upload files to Agave

All the files for the application bundle need to be uploaded before
the Agave application can be defined. The `upload-bundle.sh` should be
modified with the appropriate `SYSTEM` and `VER` values, as
adding/removing files to be uploaded. The scripts uses the Agave CLI
so you need an active token for the vdj account.

This script also contains notes for the commands used to create the
binary bundle.

### Create Agave application

The application can be created and updated with the same command.

```
$ apps-addupdate -F repcalc.json
Successfully added app repcalc-stampede2-1.0
```

Once the application is defined in Agave, the `apps-addupdate` command
only needs to be re-run when the JSON definition changes. For most
changes, you can just re-upload the binary bundle then run a test job.

### Submit test jobs

All tests should submit and run successfully. Verify that they
actually ran the application you expected. Review the log files in the
scratch directory for any errors. If you find errors, you will need to
go back and repeat appropriate steps. It is also useful for debugging
to modify the submission script in the scratch directory, then use
`idev` to interactively run and debug the app.

```
$ jobs-submit -F test-cli.json
```

### Publish application

The application resides as a development application. That is, when a
job is run, Agave copies the `deploymentPath` directory tree to the
scratch directory. Thus any changes to files in that `deploymentPath`
will change the application. For production, we want a fixed version
of the application that won't change. The `apps-publish` command does
this by making a zip archive of `deploymentPath` and putting it in the
`publicAppsDir` for the storage system, which is `/api/v2/apps` for
VDJServer. In the process it prefixes a revision number to create a
unique Agave app id.

```
$ apps-publish repcalc-stampede2-1.0
Successfully published app repcalc-stampede2-1.0u1
```

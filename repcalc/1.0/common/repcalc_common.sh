#
# VDJServer RepCalc common functions
#
# This script relies upon global variables
# source repcalc_common.sh
#
# Author: Scott Christley
# Date: Sep 1, 2016
# 

# required global variables:
# REPCALC_EXE
# PYTHON
# AGAVE_JOB_ID
# and...
# The agave app input and parameters

# the app
export APP_NAME=RepCalc

# IgBlast germline database and extra files
VDJ_DB_VERSION=10_05_2016
export IGDATA
export VDJ_DB_ROOT="$IGDATA/$VDJ_DB_VERSION/"

# ----------------------------------------------------------------------------
function expandfile () {
    fileBasename="${1%.*}" # file.txt.gz -> file.txt
    fileExtension="${1##*.}" # file.txt.gz -> gz

    if [ ! -f $1 ]; then
        echo "Could not find input file $1" 1>&2
        exit 1
    fi

    if [ "$fileExtension" == "gz" ]; then
        gunzip $1
        export file=$fileBasename
        # don't archive the intermediate file
    elif [ "$fileExtension" == "bz2" ]; then
        bunzip2 $1
        export file=$fileBasename
    elif [ "$fileExtension" == "zip" ]; then
        unzip -o $1
        export file=$fileBasename
    else
        export file=$1
    fi
}

function noArchive() {
    echo $1 >> .agave.archive
}

# ----------------------------------------------------------------------------
# Process workflow metadata
function initProcessMetadata() {
    $PYTHON ./process_metadata.py --init $APP_NAME $AGAVE_JOB_ID process_metadata.json
    if [ -f study_metadata.json ]; then
	# reformat study metadata
	$PYTHON ./process_metadata.py study_metadata.json
    fi
    noArchive "process_metadata.pyc"
    # collect all output files
    mkdir ${AGAVE_JOB_ID}
    noArchive ${AGAVE_JOB_ID}
    ARCHIVE_FILE_LIST=""
}

function addLogFile() {
    $PYTHON ./process_metadata.py --entry log "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
}

function addConfigFile() {
    $PYTHON ./process_metadata.py --entry config "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
}

function addOutputFile() {
    $PYTHON ./process_metadata.py --entry output "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
    ARCHIVE_FILE_LIST="${ARCHIVE_FILE_LIST} $4"
}

function includeFile() {
    $PYTHON ./process_metadata.py process_metadata.json --include $1
}

# ----------------------------------------------------------------------------
# RepCalc workflow

function gather_secondary_inputs() {
    # Gather secondary input files
    # This is used to get around Agave size limits for job inputs and parameters
    if [[ $SecondaryInputsFlag -eq 1 ]]; then
	echo "Gathering secondary input"
	VDJMLFiles=$(${PYTHON} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" VDJMLFilesMetadata study_metadata.json)
	VDJMLFilesMetadata=$(${PYTHON} ./process_metadata.py --getSecondaryEntry VDJMLFilesMetadata study_metadata.json)
	SummaryFiles=$(${PYTHON} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" SummaryFilesMetadata study_metadata.json)
	SummaryFilesMetadata=$(${PYTHON} ./process_metadata.py --getSecondaryEntry SummaryFilesMetadata study_metadata.json)
	ChangeOFiles=$(${PYTHON} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" ChangeOFilesMetadata study_metadata.json)
	ChangeOFilesMetadata=$(${PYTHON} ./process_metadata.py --getSecondaryEntry ChangeOFilesMetadata study_metadata.json)
    fi
}

function print_versions() {
    echo "VERSIONS:"
    echo "  $($REPCALC_EXE --version 2>&1)"
    echo -e "\nSTART at $(date)"
}

function print_parameters() {
    echo "Input files:"
    echo "ProjectDirectory=${ProjectDirectory}"
    echo "JobFiles=$JobFiles"
    echo "VDJMLFiles=${VDJMLFiles}"
    echo "SummaryFiles=${SummaryFiles}"
    echo "ChangeOFiles=${ChangeOFiles}"
    echo "StudyMetadata=${StudyMetadata}"
    echo ""
    echo "Application parameters:"
    echo "SecondaryInputsFlag=${SecondaryInputsFlag}"
    echo "JobSelected=${JobSelected}"
    echo "VDJMLFilesMetadata=${VDJMLFilesMetadata}"
    echo "SummaryFilesMetadata=${SummaryFilesMetadata}"
    echo "ChangeOFilesMetadata=${ChangeOFilesMetadata}"
    echo "Gene Segment:"
    echo "GeneSegmentFlag=${GeneSegmentFlag}"
    echo "GeneSegmentLevels=${GeneSegmentLevels}"
    echo "GeneSegmentOperations=${GeneSegmentOperations}"
    echo "GeneSegmentFilters=${GeneSegmentFilters}"
    echo "CDR3:"
    echo "CDR3Flag=${CDR3Flag}"
    echo "CDR3Levels=${CDR3Levels}"
    echo "CDR3Operations=${CDR3Operations}"
    echo "CDR3Filters=${CDR3Filters}"
    echo "Diversity:"
    echo "DiversityFlag=${DiversityFlag}"
    echo "DiversityLevels=${DiversityLevels}"
    echo "DiversityOperations=${DiversityOperations}"
    echo "DiversityFilters=${DiversityFilters}"
    echo "Clonal Analysis:"
    echo "ClonalFlag=${ClonalFlag}"
    echo "ClonalOperations=${ClonalOperations}"
    echo "ClonalFilters=${ClonalFilters}"
    echo "Mutational Analysis:"
    echo "MutationalFlag=${MutationalFlag}"
    echo "MutationalOperations=${MutationalOperations}"
    echo "MutationalFilters=${MutationalFilters}"
}

function run_repcalc_workflow() {
    initProcessMetadata
    addLogFile $APP_NAME log stdout "${AGAVE_LOG_NAME}.out" "Job Output Log" "log" null
    addLogFile $APP_NAME log stderr "${AGAVE_LOG_NAME}.err" "Job Error Log" "log" null
    addLogFile $APP_NAME log agave_log .agave.log "Agave Output Log" "log" null

    # Exclude input files from archive
    noArchive "${ProjectDirectory}"
    for file in $JobFiles; do
	if [ -f $file ]; then
	    unzip -o $file
	    noArchive $file
	    noArchive "${file%.*}"
	fi
    done

    # launcher job file
    if [ -f joblist ]; then
	echo "Warning: removing file 'joblist'.  That filename is reserved." 1>&2
	rm joblist
	touch joblist
    fi
    noArchive "joblist"

    organism=$($PYTHON ./get_parameter.py --organism study_metadata.json)
    if [ "$organism" == "mouse" ]; then
	organism="Mus_musculus"
    fi
    echo "Organism: $organism"
    seqtype=$($PYTHON ./get_parameter.py --seqtype study_metadata.json)
    echo "Seq Type: $seqtype"

    # Construct RepCalc input config file
    #echo $PYTHON ./repcalc_create_config.py --init $StudyMetadata $organism $seqtype repcalc_config.json
    #$PYTHON ./repcalc_create_config.py --init $StudyMetadata $organism $seqtype repcalc_config.json
    echo repcalc_create_config --init $StudyMetadata $organism $seqtype repcalc_config.json
    repcalc_create_config --init $StudyMetadata $organism $seqtype repcalc_config.json
    addConfigFile $APP_NAME config main "repcalc_config.json" "RepCalc Input Configuration" "json" null

    # decompress files
    fileList=($VDJMLFiles)
    count=0
    while [ "x${fileList[count]}" != "x" ]
    do
	file=${fileList[count]}
	noArchive $file
	expandfile $file
	fileOutname="${file##*/}"
	noArchive $fileOutname

	count=$(( $count + 1 ))
    done

    # decompress files
    fileList=($SummaryFiles)
    count=0
    while [ "x${fileList[count]}" != "x" ]
    do
	file=${fileList[count]}
	noArchive $file
	expandfile $file
	fileOutname="${file##*/}"
	noArchive $fileOutname

	count=$(( $count + 1 ))
    done

    # decompress files
    cloneFileList=()
    fileList=($ChangeOFiles)
    fileMetadataList=($ChangeOFilesMetadata)
    count=0
    while [ "x${fileList[count]}" != "x" ]
    do
	file=${fileList[count]}
	mfile=${fileMetadataList[count]}
	noArchive $file
	expandfile $file
	fileOutname="${file##*/}"
	noArchive $fileOutname

	# Change-O clones
        echo "export VDJ_DB_ROOT=\"$VDJ_DB_ROOT\" && export R_LIBS=\"$R_LIBS\" && bash ./do_clones.sh $fileOutname $seqtype $organism" >> joblist

        # save filenames for later processing
	fileBasename="${fileOutname%.*}"
	fileBasename="${fileBasename%.*}"
	fileName="${fileBasename}.clones.tab"
	cloneFileList[${#cloneFileList[@]}]=$fileName

	# will get compressed at end
	group="file${count}"
	addOutputFile $group $APP_NAME clones ${fileName}.zip "${fileBasename} Change-O Clones" "tsv" $mfile

	count=$(( $count + 1 ))
    done

    # check number of jobs to be run
    numJobs=$(cat joblist | wc -l)
    export LAUNCHER_PPN=$LAUNCHER_MID_PPN
    if [ $numJobs -lt $LAUNCHER_PPN ]; then
	export LAUNCHER_PPN=$numJobs
    fi

    # run launcher
    $LAUNCHER_DIR/paramrun
    rm joblist
    touch joblist

    # add files to input config
    vdjmlMeta=($VDJMLFilesMetadata)
    summaryMeta=($SummaryFilesMetadata)
    changeoMeta=($ChangeOFilesMetadata)

    count=0
    while [ "x${vdjmlMeta[count]}" != "x" ]
    do
	filekey="file${count}"
	#$PYTHON ./repcalc_create_config.py repcalc_config.json --file $filekey ${vdjmlMeta[count]} ${summaryMeta[count]} ${changeoMeta[count]}
	repcalc_create_config repcalc_config.json --file $filekey ${vdjmlMeta[count]} ${summaryMeta[count]} ${changeoMeta[count]}
	count=$(( $count + 1 ))
    done

    # add samples to input config
    if [ -n "$JobSelected" ]; then
	#$PYTHON ./repcalc_create_config.py repcalc_config.json --samples ${JobSelected}
	repcalc_create_config repcalc_config.json --samples ${JobSelected}
    fi

    # Gene segment usage
    if [[ $GeneSegmentFlag -eq 1 ]]; then
	ARGS=""
	if [ -n "$GeneSegmentLevels" ]; then
	    ARGS="${ARGS} --geneSegmentLevels $GeneSegmentLevels"
	fi
	if [ -n "$GeneSegmentOperations" ]; then
	    ARGS="${ARGS} --geneSegmentOperations $GeneSegmentOperations"
	fi
	if [ -n "$GeneSegmentFilters" ]; then
	    ARGS="${ARGS} --geneSegmentFilters $GeneSegmentFilters"
	fi
	cp repcalc_config.json repcalc_segment_config.json
	#$PYTHON ./repcalc_create_config.py repcalc_segment_config.json $ARGS
	repcalc_create_config repcalc_segment_config.json $ARGS
	addConfigFile $APP_NAME config segment "repcalc_segment_config.json" "RepCalc Input Configuration" "json" null

        # Run RepCalc
	echo "Calculate gene segment usage"
	$REPCALC_EXE repcalc_segment_config.json --output output_repcalc_segment_config.json
	if [ $? -ne 0 ]; then
	    export JOB_ERROR=1
	fi
	includeFile output_repcalc_segment_config.json
	noArchive output_repcalc_segment_config.json
    fi

    # CDR3
    if [[ $CDR3Flag -eq 1 ]]; then
	rm -f joblist
	touch joblist

	ARGS=""
	if [ -n "$CDR3Levels" ]; then
	    ARGS="${ARGS} --cdr3Levels $CDR3Levels"
	fi
	if [ -n "$CDR3Operations" ]; then
	    ARGS="${ARGS} --cdr3Operations $CDR3Operations"
	fi
	if [ -n "$CDR3Filters" ]; then
	    ARGS="${ARGS} --cdr3Filters $CDR3Filters"
	fi

	# generate single group configs
	#scriptList=$($PYTHON ./repcalc_create_config.py repcalc_config.json --cdr3Single)
	scriptList=$(repcalc_create_config repcalc_config.json --cdr3Single)
	#echo $scriptList

	for script in ${scriptList[@]}; do
	    #$PYTHON ./repcalc_create_config.py $script $ARGS
	    repcalc_create_config $script $ARGS

	    echo "$REPCALC_EXE $script --output output_${script}" >> joblist
	    noArchive ${script}
	done

        # check number of jobs to be run
	numJobs=$(cat joblist | wc -l)
	export LAUNCHER_PPN=$LAUNCHER_MAX_PPN
	if [ $numJobs -lt $LAUNCHER_PPN ]; then
	    export LAUNCHER_PPN=$numJobs
	fi

        # run launcher
	echo "CDR3 calculations"
	$LAUNCHER_DIR/paramrun
	rm -f joblist
	touch joblist

	# add CDR3 output to provenance
	for script in ${scriptList[@]}; do
	    includeFile output_${script}
	    noArchive output_${script}
	done

	# generate pairwise comparison
	check=$(echo "${CDR3Operations}" | grep -o "shared" | wc -l)
	if [[ $check -eq 1 ]]; then
	    ARGS=""
	    if [ -n "$CDR3Levels" ]; then
		ARGS="${ARGS} --cdr3Levels $CDR3Levels"
	    fi
	    ARGS="${ARGS} --cdr3Operations compare"
	    if [ -n "$CDR3Filters" ]; then
		ARGS="${ARGS} --cdr3Filters $CDR3Filters"
	    fi

	    # generate configs
	    #fileCompareScripts=$($PYTHON ./repcalc_create_config.py repcalc_config.json --cdr3CompareFiles)
	    fileCompareScripts=$(repcalc_create_config repcalc_config.json --cdr3CompareFiles)
	    for script in ${fileCompareScripts[@]}; do
		#echo "$PYTHON ./repcalc_create_config.py $script $ARGS" >> joblist
		echo "repcalc_create_config $script $ARGS" >> joblist
		noArchive ${script}
	    done

	    #sampleCompareScripts=$($PYTHON ./repcalc_create_config.py repcalc_config.json --cdr3CompareSamples)
	    sampleCompareScripts=$(repcalc_create_config repcalc_config.json --cdr3CompareSamples)
	    for script in ${sampleCompareScripts[@]}; do
		#echo "$PYTHON ./repcalc_create_config.py $script $ARGS" >> joblist
		echo "repcalc_create_config $script $ARGS" >> joblist
		noArchive ${script}
	    done

	    #groupCompareScripts=$($PYTHON ./repcalc_create_config.py repcalc_config.json --cdr3CompareGroups)
	    groupCompareScripts=$(repcalc_create_config repcalc_config.json --cdr3CompareGroups)
	    for script in ${groupCompareScripts[@]}; do
		#echo "$PYTHON ./repcalc_create_config.py $script $ARGS" >> joblist
		echo "repcalc_create_config $script $ARGS" >> joblist
		noArchive ${script}
	    done

            # check number of jobs to be run
	    numJobs=$(cat joblist | wc -l)
	    export LAUNCHER_PPN=$LAUNCHER_MAX_PPN
	    if [ $numJobs -lt $LAUNCHER_PPN ]; then
		export LAUNCHER_PPN=$numJobs
	    fi

            # run launcher
	    echo "CDR3 pairwise comparison configs"
	    $LAUNCHER_DIR/paramrun
	    rm -f joblist
	    touch joblist

	    scriptList="${fileCompareScripts[@]} ${sampleCompareScripts[@]}"
	    #echo ${scriptList[@]}

	    # run repcalc
	    for script in ${scriptList[@]}; do
		echo "$REPCALC_EXE $script" >> joblist
	    done

            # check number of jobs to be run
	    numJobs=$(cat joblist | wc -l)
	    export LAUNCHER_PPN=$LAUNCHER_MAX_PPN
	    if [ $numJobs -lt $LAUNCHER_PPN ]; then
		export LAUNCHER_PPN=$numJobs
	    fi

            # run launcher
	    echo "CDR3 pairwise comparison calculations"
	    $LAUNCHER_DIR/paramrun
	    rm -f joblist
	    touch joblist

	    # run repcalc for sample groups separately to give more memory
	    scriptList="${groupCompareScripts[@]}"

	    # run repcalc
	    for script in ${scriptList[@]}; do
		echo "$REPCALC_EXE $script" >> joblist
	    done

            # check number of jobs to be run
	    numJobs=$(cat joblist | wc -l)
	    export LAUNCHER_PPN=$LAUNCHER_LOW_PPN
	    if [ $numJobs -lt $LAUNCHER_PPN ]; then
		export LAUNCHER_PPN=$numJobs
	    fi

            # run launcher
	    echo "CDR3 pairwise comparison calculations"
	    $LAUNCHER_DIR/paramrun

	    # concatenate all the individual files
            # make sure to do sampleGroup before sample, so they don't get mixed
	    echo "Concatenate CDR3 pairwise comparison calculations"
	    for group_name in sampleGroup file sample; do
		for calc_type in aa v_aa vj_aa nucleotide v_nucleotide vj_nucleotide; do
		    for f in ${group_name}*_${group_name}*_comparison_cdr3_${calc_type}_sharing.tsv; do
			rm -f group_${group_name}_comparison_cdr3_${calc_type}_sharing.tsv
			cat ${group_name}*_${group_name}*_comparison_cdr3_${calc_type}_sharing.tsv > group_${group_name}_comparison_cdr3_${calc_type}_sharing.tsv
			rm -rf ${group_name}*_${group_name}*_comparison_cdr3_${calc_type}_sharing.tsv
			#addOutputFile $APP_NAME cdr3_shared group_${group_name}_comparison_cdr3_${calc_type} group_${group_name}_comparison_cdr3_${calc_type}_sharing.tsv "Shared CDR3" "tsv" null
			noArchive group_${group_name}_comparison_cdr3_${calc_type}_sharing.tsv
			break
		    done
		    for f in ${group_name}*_${group_name}*_diff_cdr3_${calc_type}_sharing.tsv; do
			rm -f group_${group_name}_diff_cdr3_${calc_type}_sharing.tsv
			cat ${group_name}*_${group_name}*_diff_cdr3_${calc_type}_sharing.tsv > group_${group_name}_diff_cdr3_${calc_type}_sharing.tsv
			rm -rf ${group_name}*_${group_name}*_diff_cdr3_${calc_type}_sharing.tsv
			#addOutputFile $APP_NAME cdr3_shared group_${group_name}_diff_cdr3_${calc_type} group_${group_name}_diff_cdr3_${calc_type}_sharing.tsv "Unique CDR3" "tsv" null
			noArchive group_${group_name}_diff_cdr3_${calc_type}_sharing.tsv
			break
		    done
		done
	    done
	fi
    fi

    # Diversity analysis
    if [[ $DiversityFlag -eq 1 ]]; then
	rm -f joblist
	touch joblist

	scriptList=$($PYTHON ./create_r_scripts.py repcalc_config.json process_metadata.json --diversity diversity_analysis)
	#echo ${scriptList[@]}
	for script in ${scriptList[@]}; do
	    echo "export R_LIBS=\"$R_LIBS\" && R --quiet --no-save < ${script}" >> joblist
	    noArchive ${script}
	done
	#cat joblist

        # check number of jobs to be run
	numJobs=$(cat joblist | wc -l)
	export LAUNCHER_PPN=$LAUNCHER_MID_PPN
	if [ $numJobs -lt $LAUNCHER_PPN ]; then
	    export LAUNCHER_PPN=$numJobs
	fi

        # run launcher
	$LAUNCHER_DIR/paramrun
    fi

    # Clonal analysis
    if [[ $ClonalFlag -eq 1 ]]; then
	rm -f joblist
	touch joblist

	scriptList=$($PYTHON ./create_r_scripts.py repcalc_config.json process_metadata.json --clonal clonal_analysis)
	#echo ${scriptList[@]}
	for script in ${scriptList[@]}; do
	    echo "export R_LIBS=\"$R_LIBS\" && R --quiet --no-save < ${script}" >> joblist
	    noArchive ${script}
	done
	#cat joblist

        # check number of jobs to be run
	numJobs=$(cat joblist | wc -l)
	export LAUNCHER_PPN=$LAUNCHER_MID_PPN
	if [ $numJobs -lt $LAUNCHER_PPN ]; then
	    export LAUNCHER_PPN=$numJobs
	fi

        # run launcher
	$LAUNCHER_DIR/paramrun
    fi

    # Mutational analysis
    if [[ $MutationalFlag -eq 1 ]]; then
	rm -f joblist
	touch joblist

	scriptList=$($PYTHON ./create_r_scripts.py repcalc_config.json process_metadata.json --mutational mutational_analysis)
	#echo ${scriptList[@]}
	for script in ${scriptList[@]}; do
	    echo "export R_LIBS=\"$R_LIBS\" && R --quiet --no-save < ${script}" >> joblist
	    noArchive ${script}
	done
	#cat joblist

	# mutational analysis is memory intensive so use the low setting
        # check number of jobs to be run
	numJobs=$(cat joblist | wc -l)
	export LAUNCHER_PPN=$LAUNCHER_LOW_PPN
	if [ $numJobs -lt $LAUNCHER_PPN ]; then
	    export LAUNCHER_PPN=$numJobs
	fi

        # run launcher
	$LAUNCHER_DIR/paramrun
    fi

    # compress
    for file in ${cloneFileList[@]}; do
	zip ${file}.zip ${file}
	rm ${file}

	# compress collapsed clone files if they exist, add to archive
	fileBasename="${file%.*}"
	fileBasename="${fileBasename%.*}"
	fileName="${fileBasename}.clones.collapse.tsv"
	if [ -f $fileName ]; then
	    zip ${fileName}.zip ${fileName}
	    ARCHIVE_FILE_LIST="${ARCHIVE_FILE_LIST} ${fileName}.zip"
	    rm ${fileName}
	fi
    done

    # chart data files
    # the for loop is simple technique to check for wildcard files
    for f in *_segment_counts.json; do
	zip segment_counts_data.zip *_segment_counts.json
	addOutputFile $APP_NAME gene_segment_usage chart_data segment_counts_data.zip "Gene Segment Usage chart data" "json" null
	break
    done
    for f in *_segment_combos.tsv; do
	zip segment_combos_data.zip *_segment_combos.tsv
	addOutputFile $APP_NAME gene_segment_combos chart_data segment_combos_data.zip "Gene Segment Combinations chart data" "json" null
	break
    done
    for f in *_cdr3_*_length.tsv; do
	zip cdr3_length_data.zip *_cdr3_*_length.tsv
	addOutputFile $APP_NAME cdr3_length chart_data cdr3_length_data.zip "CDR3 Length Histogram chart data" "tsv" null
	break
    done
    for f in *_cdr3_*_sharing.tsv; do
	zip cdr3_sharing_data.zip *_cdr3_*_sharing.tsv
	addOutputFile $APP_NAME cdr3_sharing chart_data cdr3_sharing_data.zip "Shared/Unique CDR3 chart data" "tsv" null
	break
    done
    for f in *.clones.abundance.*.tsv; do
	zip clonal_abundance_data.zip *.clones.abundance.*.tsv
	addOutputFile $APP_NAME clonal_abundance chart_data clonal_abundance_data.zip "Clonal Abundance chart data" "tsv" null
	break
    done
    for f in *.clones.diversity.*.tsv; do
	zip diversity_curve_data.zip *.clones.diversity.*.tsv
	addOutputFile $APP_NAME diversity_curve chart_data diversity_curve_data.zip "Diversity Curve chart data" "tsv" null
	break
    done
    for f in *.clones.mutational.*.tsv; do
	zip selection_pressure_data.zip *.clones.mutational.*.tsv
	addOutputFile $APP_NAME selection_pressure chart_data selection_pressure_data.zip "Selection Pressure chart data" "tsv" null
	break
    done

    # zip archive of all output files
    for file in $ARCHIVE_FILE_LIST; do
	if [ -f $file ]; then
	    cp -f $file ${AGAVE_JOB_ID}
	fi
    done
    zip ${AGAVE_JOB_ID}.zip ${AGAVE_JOB_ID}/*
    addLogFile $APP_NAME log output_archive ${AGAVE_JOB_ID}.zip "Archive of Output Files" "zip" null
}

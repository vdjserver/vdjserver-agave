#
# Generate JSON config file for RepCalc
#
# Author: Scott Christley
# Date: Sep 16, 2016
#

from __future__ import print_function
import json
import argparse
import sys
import copy

def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

def restrictByLogical(sampleList, studyMetadata, sampleGroup):
    newList = []
    for sampleKey in sampleList:
        sample = studyMetadata['samples'][sampleKey]
        # get the field value
        logicalSplit = sampleGroup['value']['logical_field'].split('.')
        if logicalSplit[0] == 'sample': fieldValue = sample['value'][logicalSplit[1]]
        elif logicalSplit[0] == 'subject':
            subjectKey = sample['value'].get('subject_id')
            if subjectKey: subject = studyMetadata['subjects'][subjectKey]
            else:
                print('WARNING: sample group (', sampleGroup['uuid'],
                      ') has logical on (', sampleGroup['value']['logical_field'],
                      ') but sample (', sample['uuid'],
                      ') does not have associated subject metadata')
                continue
            fieldValue = subject['value'][logicalSplit[1]]
        else:
            print('WARNING: sample group (', sampleGroup['uuid'],
                  ') has unknown logical field (', sampleGroup['value']['logical_field'], ')')
            continue
        # do the check
        #print('logical: ', fieldValue)
        result = False
        if sampleGroup['value']['logical_operator'] == '=': result = (fieldValue == sampleGroup['value']['logical_value'])
        if sampleGroup['value']['logical_operator'] == '!=': result = (fieldValue != sampleGroup['value']['logical_value'])
        if sampleGroup['value']['logical_operator'] == '<': result = (fieldValue < sampleGroup['value']['logical_value'])
        if sampleGroup['value']['logical_operator'] == '>': result = (fieldValue > sampleGroup['value']['logical_value'])
        if sampleGroup['value']['logical_operator'] == '>=': result = (fieldValue >= sampleGroup['value']['logical_value'])
        if sampleGroup['value']['logical_operator'] == '<=': result = (fieldValue <= sampleGroup['value']['logical_value'])
        if sampleGroup['value']['logical_operator'] == 'contains': result = (fieldValue.find(sampleGroup['value']['logical_value']) >= 0)
        # passed the test
        if result: newList.append(sampleKey)
    return newList

def groupByCategory(sampleList, studyMetadata, sampleGroup):
    categorySplit = sampleGroup['value']['category'].split('.')
    groupBy = {}
    for sampleKey in sampleList:
        sample = studyMetadata['samples'][sampleKey]
        if categorySplit[0] == 'sample': fieldValue = sample['value'][categorySplit[1]]
        elif categorySplit[0] == 'subject':
            subjectKey = sample['value'].get('subject_id')
            if subjectKey: subject = studyMetadata['subjects'][subjectKey]
            else:
                print('WARNING: sample group (', sampleGroup['uuid'],
                      ') has category on (', sampleGroup['value']['category'],
                      ') but sample (', sample['uuid'],
                      ') does not have associated subject metadata')
                continue
            fieldValue = subject['value'][categorySplit[1]]
        else:
            print('WARNING: sample group (', sampleGroup['uuid'],
                  ') has unknown category field (', sampleGroup['value']['category'], ')')
            continue
        #print('category: ', fieldValue)
        if not groupBy.get(fieldValue): groupBy[fieldValue] = [ ]
        group = groupBy[fieldValue]
        group.append(sampleKey)
    return groupBy

# generate map of original source files
derivationMap = {}
def initDerivationMap(studyMetadata, jobMetadata):
    for file in jobMetadata['value']['files']:
        # summary file
        jobFile = jobMetadata['value']['files'][file].get('summary')
        if jobFile and jobFile.get('WasDerivedFrom') and jobFile['WasDerivedFrom'] is not None:
            sourceFile = wasDerivedFrom(studyMetadata, jobMetadata, jobFile['WasDerivedFrom'])
            if sourceFile: derivationMap[jobFile['value']] = sourceFile['uuid']
    #print(derivationMap)

def wasDerivedFrom(studyMetadata, processMetadata, fileId):
    fileMetadata = studyMetadata['fileMetadata'].get(fileId)
    if not fileMetadata:
        #print(fileId)
        # could be reference to group
        # TODO: this is specialized for the single use case, file merging in vdjpipe and presto
        if processMetadata['value']['groups'].get(fileId):
            if processMetadata['value']['groups'][fileId].get(processMetadata['value']['process']['appName']):
                file = processMetadata['value']['groups'][fileId][processMetadata['value']['process']['appName']]['files']
                fileEntry = processMetadata['value']['files'].get(file)
                if fileEntry and len(fileEntry.keys()) == 1:
                    fileType = fileEntry.keys()[0]
                    jobFile = processMetadata['value']['files'][file][fileType]
                    if jobFile.get('WasDerivedFrom') and jobFile['WasDerivedFrom'] is not None:
                        return wasDerivedFrom(studyMetadata, processMetadata, jobFile['WasDerivedFrom'])
    elif fileMetadata['name'] == 'projectFile':
        # found original source
        return fileMetadata
    elif fileMetadata['name'] == 'projectJobFile':
        # found job file, continue up WasDerivedFrom links
        jobId = fileMetadata['value']['jobUuid']
        jobMetadata = studyMetadata['processMetadata'][jobId]
        for file in jobMetadata['value']['files']:
            for fileType in jobMetadata['value']['files'][file]:
                jobFile = jobMetadata['value']['files'][file][fileType]
                if jobFile['value'] == fileMetadata['value']['name']:
                    if jobFile.get('WasDerivedFrom') and jobFile['WasDerivedFrom'] is not None:
                        return wasDerivedFrom(studyMetadata, jobMetadata, jobFile['WasDerivedFrom'])
    return None


#
# main routine
#
template = { "groups": {}, "files": {}, "calculations": [] }

parser = argparse.ArgumentParser(description='Generate RepCalc config.')
parser.add_argument('--init', type=str, nargs=3, help='Create initial config with metadata file', metavar=('metadata file', 'organism', 'seqtype'))
parser.add_argument('json_file', type=str, help='Output JSON file name')

parser.add_argument('--geneSegmentLevels', type=str, nargs='*', help='Gene segment levels')
#parser.add_argument('--geneSegmentSummarizeBy', type=str, nargs='*', help='Gene segment summarize by')
parser.add_argument('--geneSegmentOperations', type=str, nargs='*', help='Gene segment operations')
parser.add_argument('--geneSegmentFilters', type=str, nargs='*', help='Gene segment filters')

parser.add_argument('--cdr3Levels', type=str, nargs='*', help='CDR3 levels')
parser.add_argument('--cdr3Operations', type=str, nargs='*', help='CDR3 operations')
parser.add_argument('--cdr3Filters', type=str, nargs='*', help='CDR3 filters')

parser.add_argument('--cdr3Single', action='store_true', help='Generate CDR3 single group config files')
parser.add_argument('--cdr3CompareFiles', action='store_true', help='Generate CDR3 pairwise comparison config for files')
parser.add_argument('--cdr3CompareSamples', action='store_true', help='Generate CDR3 pairwise comparison config for samples')
parser.add_argument('--cdr3CompareGroups', action='store_true', help='Generate CDR3 pairwise comparison config for sample groups')

parser.add_argument('--file', type=str, nargs=4, help='File set', metavar=('filekey', 'vdjml', 'summary', 'changeo'))

parser.add_argument('--samples', type=str, help='Add samples', metavar=('jobId'))

args = parser.parse_args()
if args:
    if args.init:
        # save the json
        template['metadata'] = args.init[0];
        template['organism'] = args.init[1];
        template['seqType'] = args.init[2];
        with open(args.json_file, 'w') as json_file:
            json.dump(template, json_file)

    # load json
    with open(args.json_file, 'r') as f:
        config = json.load(f)

    # gene segment usage
    if args.geneSegmentOperations:
        if not args.geneSegmentLevels:
            config['calculations'].append({"type":"gene segment usage",
                                           "levels":[],
                                           "operations":args.geneSegmentOperations,
                                           "filters":args.geneSegmentFilters});
        else:
            config['calculations'].append({"type":"gene segment usage",
                                           "levels":args.geneSegmentLevels,
                                           "operations":args.geneSegmentOperations,
                                           "filters":args.geneSegmentFilters});

    # CDR3
    if args.cdr3Levels:
        if not args.cdr3Operations:
            print ("ERROR: cdr3Operations not specified.", file=sys.stderr);
            sys.exit(1);

        config['calculations'].append({"type":"CDR3",
                                       "levels":args.cdr3Levels,
                                       "operations":args.cdr3Operations,
                                       "filters":args.cdr3Filters});

    # files
    if args.file:
        config['files'][args.file[0]] = { "vdjml": { "value": args.file[1] },
                                          "summary": { "value": args.file[2] },
                                          "changeo": { "value": args.file[3] } }
        config['groups'][args.file[0]] = { "type": "file", "files": [ args.file[0] ] }

    # samples and sample groups
    if args.samples:
        with open(config['metadata'], 'r') as f:
            studyMetadata = json.load(f)

        jobMetadata = studyMetadata['processMetadata'][args.samples]
        initDerivationMap(studyMetadata, jobMetadata)
        #print(derivationMap)
        #print(jobMetadata)

        # samples
        sampleList = []
        sampleFiles = {}
        sampleCount = 0
        for key in studyMetadata['samples']:
            #print (key)
            projectFile = studyMetadata['samples'][key]['value'].get('project_file')
            if not projectFile: continue
            #print(projectFile)

            # we can only include the samples for which we have data files in the job
            fileList = []
            for file in derivationMap:
                if derivationMap[file] == projectFile:
                    for groupFile in config['files']:
                        groupSummary = config['files'][groupFile].get('summary')
                        if not groupSummary: continue
                        summaryMetadata = studyMetadata['fileMetadata'][groupSummary['value']]
                        if file == summaryMetadata['value']['name']:
                            fileList.append(groupFile)
            #print(fileList)
            if len(fileList) > 0:
                sampleName = 'sample' + str(sampleCount)
                sampleList.append(key)
                sampleFiles[key] = fileList
                sampleCount += 1
                config['groups'][sampleName] = { "type": "sample", "samples": { key: fileList } }

        # sample groups
        #print(sampleList)
        sampleSet = set(sampleList)
        groupCount = 0
        for key in studyMetadata['sampleGroups']:
            sampleGroup = studyMetadata['sampleGroups'][key]
            # determine base sample list
            if len(sampleGroup['value']['samples']) > 0: sList = sampleGroup['value']['samples']
            else: sList = sampleList
            # restrict by logical
            if len(sampleGroup['value']['logical_field']) > 0:
                sList = restrictByLogical(sList, studyMetadata, sampleGroup)
            # exclude samples not available in input
            groupSet = set(sList)
            iSet = groupSet & sampleSet
            if len(iSet) != len(groupSet):
                print('WARNING: Not all sample data available for sample group: ', key)
                sList = list(iSet)
            # group by category
            groupBy = None
            if len(sampleGroup['value']['category']) > 0:
                groupBy = groupByCategory(sList, studyMetadata, sampleGroup)
                for groupKey in groupBy:
                    groupName = 'sampleGroup' + str(groupCount)
                    #print(groupName)
                    for aKey in groupBy[groupKey]:
                        anEntry = config['groups'].get(groupName)
                        if not anEntry: config['groups'][groupName] = { "type": "sampleGroup",
                                                                        "sampleGroup": sampleGroup['uuid'],
                                                                        "category": groupKey,
                                                                        "samples": { aKey: sampleFiles[aKey] } }
                        else: anEntry['samples'][aKey] = sampleFiles[aKey]
                    groupCount += 1
            else:
                groupName = 'sampleGroup' + str(groupCount)
                #print(groupName)
                for aKey in sList:
                    anEntry = config['groups'].get(groupName)
                    if not anEntry: config['groups'][groupName] = { "type": "sampleGroup", "sampleGroup": sampleGroup['uuid'], "samples": { aKey: sampleFiles[aKey] } }
                    else: anEntry['samples'][aKey] = sampleFiles[aKey]
                groupCount += 1

    # Separate all the groups into individual files so they can be run in parallel
    if args.cdr3Single:
        for key in config['groups']:
            customConfig = copy.deepcopy(config)
            customConfig['groups'] = {}
            customConfig['groups'][key] = config['groups'][key]
            output_json = key + '_cdr3_' + args.json_file
            with open(output_json, 'w') as json_file:
                json.dump(customConfig, json_file, indent=2)
            # not for debugging, produce list of scripts for calling program
            print(output_json)

    #
    # Create pairwise comparison
    #
    if args.cdr3CompareFiles:
        # files
        keys = config['groups'].keys()
        for key1 in keys:
            if config['groups'][key1]['type'] != 'file': continue
            idx = keys.index(key1)
            for key2 in keys[idx:]:
                if key1 == key2: continue
                if config['groups'][key2]['type'] != 'file': continue
                customConfig = copy.deepcopy(config)
                customConfig['groups'] = {}
                customConfig['groups'][key1] = config['groups'][key1]
                customConfig['groups'][key2] = config['groups'][key2]
                output_json = key1 + '_' + key2 + '_cdr3_' + args.json_file
                with open(output_json, 'w') as json_file:
                    json.dump(customConfig, json_file, indent=2)
                # not for debugging, produce list of scripts for calling program
                print(output_json)

    if args.cdr3CompareSamples:
        # samples
        keys = config['groups'].keys()
        for key1 in keys:
            if config['groups'][key1]['type'] != 'sample': continue
            idx = keys.index(key1)
            for key2 in keys[idx:]:
                if key1 == key2: continue
                if config['groups'][key2]['type'] != 'sample': continue
                customConfig = copy.deepcopy(config)
                customConfig['groups'] = {}
                customConfig['groups'][key1] = config['groups'][key1]
                customConfig['groups'][key2] = config['groups'][key2]
                output_json = key1 + '_' + key2 + '_cdr3_' + args.json_file
                with open(output_json, 'w') as json_file:
                    json.dump(customConfig, json_file, indent=2)
                # not for debugging, produce list of scripts for calling program
                print(output_json)

    if args.cdr3CompareGroups:
        # sample groups
        keys = config['groups'].keys()
        for key1 in keys:
            if config['groups'][key1]['type'] != 'sampleGroup': continue
            idx = keys.index(key1)
            for key2 in keys[idx:]:
                if key1 == key2: continue
                if config['groups'][key2]['type'] != 'sampleGroup': continue
                customConfig = copy.deepcopy(config)
                customConfig['groups'] = {}
                customConfig['groups'][key1] = config['groups'][key1]
                customConfig['groups'][key2] = config['groups'][key2]
                output_json = key1 + '_' + key2 + '_cdr3_' + args.json_file
                with open(output_json, 'w') as json_file:
                    json.dump(customConfig, json_file, indent=2)
                # not for debugging, produce list of scripts for calling program
                print(output_json)

    # save the json
    with open(args.json_file, 'w') as json_file:
        json.dump(config, json_file, indent=2)

else:
    # invalid arguments
    parser.print_help()


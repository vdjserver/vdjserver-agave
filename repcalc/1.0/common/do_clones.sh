#
# Environment setup and run Change-O's DefineClones and CreateGermlines
#
# Because Change-O uses python3 while VDJML requires python2,
# the two don't play together well, so this script sets up
# the python3 environment in a separate script.
#

# save in case it gets overwritten with the module changes
HOLD_R_LIBS=$R_LIBS

export PYTHONPATH=
module purge
module load TACC
module unload python
module -q load gcc
module load python3
export PYTHONPATH=$CHANGEO_PYTHON:$PYTHONPATH

CHANGEO_FILE=$1
seqType=$2
if [ "$seqType" == "TCR" ]; then seqType="TR"; fi
if [ "$seqType" == "Ig" ]; then seqType="IG"; fi
organism=$3
if [ "$organism" == "mouse" ]; then organism="Mus_musculus"; fi

fileBasename="${CHANGEO_FILE%.*}"
fileName="${fileBasename%.*}"
ParseDb.py select -d ${CHANGEO_FILE} -f FUNCTIONAL -u T
echo ${fileBasename}_parse-select.tab >> .agave.archive

# assigning clones
DefineClones.py bygroup -d ${fileBasename}_parse-select.tab --act set --model ham --sym min --norm len --dist 0.165 --nproc 1
echo ${fileBasename}_parse-select_clone-pass.tab >> .agave.archive

# create germlines
CreateGermlines.py -d ${fileBasename}_parse-select_clone-pass.tab -r $VDJ_DB_ROOT/${organism}/ReferenceDirectorySet/${seqType}_VDJ.fna -g dmask --cloned
mv ${fileBasename}_parse-select_clone-pass_germ-pass.tab ${fileName}.clones.tab

# calculate CDR3 AA properties
module purge
module load TACC
module load Rstats

export R_LIBS=$HOLD_R_LIBS:$R_LIBS

echo "library(ggplot2)" >> ${fileName}.analysis.R
echo "library(alakazam)" >> ${fileName}.analysis.R
echo "db <- readChangeoDb('"${fileName}".clones.tab')" >> ${fileName}.analysis.R
echo "db_props <- aminoAcidProperties(db, seq='JUNCTION', nt=TRUE, trim=TRUE, label='CDR3')" >> ${fileName}.analysis.R
echo "writeChangeoDb(db_props, '"${fileName}".clones.tab')" >> ${fileName}.analysis.R
echo "q()" >> ${fileName}.analysis.R
R --quiet --no-save < ${fileName}.analysis.R
echo ${fileName}.analysis.R >> .agave.archive

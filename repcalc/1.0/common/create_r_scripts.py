#
# Generate R scripts
#
# Author: Scott Christley
# Date: Dec 7, 2016
#

from __future__ import print_function
import json
import argparse
import repsum
import repsum.metadata
from process_metadata import addGroupFileEntry

#
# main routine
#

parser = argparse.ArgumentParser(description='Generate R scripts.')
parser.add_argument('json_file', type=str, help='RepCalc config JSON file name')
parser.add_argument('process_metadata', type=str, help='Process Workflow Metadata JSON file name')
parser.add_argument('--clonal', type=str, nargs=1, help='Output R script file name prefix for Alakazam clonal analysis', metavar=('r_file'))
parser.add_argument('--diversity', type=str, nargs=1, help='Output R script file name prefix for Alakazam diversity analysis', metavar=('r_file'))
parser.add_argument('--mutational', type=str, nargs=1, help='Output R script file name prefix for Shazam mutational analysis', metavar=('r_file'))

args = parser.parse_args()
if (args):
    # load repcalc config
    with open(args.json_file, 'r') as f:
        config = json.load(f)

    # load study metadata
    with open(config['metadata'], 'r') as f:
        studyMetadata = json.load(f)

    # load process metadata
    with open(args.process_metadata, 'r') as f:
        metadata = json.load(f)

    # clonal abundance R script
    if (args.clonal):
        for key in config['groups']:
            scriptName = None
            if config['groups'][key]['type'] == 'sample':
                for sample in config['groups'][key]['samples']:
                    scriptName = args.clonal[0] + '_' + key + '.R'
                    # extract filename
                    fileKey = config['groups'][key]['samples'][sample][0]

            if config['groups'][key]['type'] == 'file':
                scriptName = args.clonal[0] + '_' + key + '.R'
                fileKey = config['groups'][key]['files'][0]

            if scriptName:
                # not for debugging, produce list of scripts for calling program
                print(scriptName)

                # extract filename
                fileUuid = config['files'][fileKey]['changeo']['value']
                fileMetadata = studyMetadata['fileMetadata'][fileUuid]
                fileName = repsum.metadata.filename(fileMetadata)

                # chop off changeo prefixes
                sname = fileName.split('.')
                del sname[-1]
                del sname[-1]
                fileBase = '.'.join(sname)
                fileName = fileBase + ".clones.tab"
                fileOut = fileBase + ".clones.abundance." + key + ".tsv"

                with open(scriptName, 'w') as r_file:
                    r_file.write('library(ggplot2)\n')
                    r_file.write('library(alakazam)\n')
                    r_file.write('db <- readChangeoDb("' + fileName + '")\n')
                    r_file.write("clones <- estimateAbundance(db, group='FUNCTIONAL', ci=0.95, nboot=200)\n")
                    r_file.write("write.table(clones, row.names=F, sep='\\t', file='" + fileOut + "')\n")
                    r_file.write("q()\n")
                    addGroupFileEntry(metadata, "output", key, "clonal_abundance",
                                      "clonal_abundance", fileOut, "Clonal Abundance", "tsv", fileUuid)

    # diversity curve R script
    if (args.diversity):
        for key in config['groups']:
            scriptName = None
            if config['groups'][key]['type'] == 'sample':
                for sample in config['groups'][key]['samples']:
                    scriptName = args.diversity[0] + '_' + key + '.R'
                    fileKey = config['groups'][key]['samples'][sample][0]

            if config['groups'][key]['type'] == 'file':
                scriptName = args.diversity[0] + '_' + key + '.R'
                fileKey = config['groups'][key]['files'][0]

            if scriptName:
                # not for debugging, produce list of scripts for calling program
                print(scriptName)

                # extract filename
                fileUuid = config['files'][fileKey]['changeo']['value']
                fileMetadata = studyMetadata['fileMetadata'][fileUuid]
                fileName = repsum.metadata.filename(fileMetadata)

                # chop off changeo prefixes
                sname = fileName.split('.')
                del sname[-1]
                del sname[-1]
                fileBase = '.'.join(sname)
                fileName = fileBase + ".clones.tab"
                fileOut = fileBase + ".clones.diversity." + key + ".tsv"

                with open(scriptName, 'w') as r_file:
                    r_file.write('library(ggplot2)\n')
                    r_file.write('library(alakazam)\n')
                    r_file.write('db <- readChangeoDb("' + fileName + '")\n')
                    r_file.write("sample_div <- rarefyDiversity(db, 'FUNCTIONAL', min_q=0, max_q=32, step_q=0.05, ci=0.95, nboot=200)\n")
                    r_file.write("write.table(sample_div@data, row.names=F, sep='\\t', file='" + fileOut + "')\n")
                    r_file.write("q()\n")
                    addGroupFileEntry(metadata, "output", key, "diversity_curve",
                                      "diversity_curve", fileOut, "Diversity Curve", "tsv", fileUuid)

    # mutational analysis R scripts
    if (args.mutational):
        for key in config['groups']:
            scriptName = None
            if config['groups'][key]['type'] == 'sample':
                for sample in config['groups'][key]['samples']:
                    scriptName = args.mutational[0] + '_' + key + '.R'
                    fileKey = config['groups'][key]['samples'][sample][0]

            if config['groups'][key]['type'] == 'file':
                scriptName = args.mutational[0] + '_' + key + '.R'
                fileKey = config['groups'][key]['files'][0]

            if scriptName:
                # not for debugging, produce list of scripts for calling program
                print(scriptName)

                # extract filename
                fileUuid = config['files'][fileKey]['changeo']['value']
                fileMetadata = studyMetadata['fileMetadata'][fileUuid]
                fileName = repsum.metadata.filename(fileMetadata)

                # chop off changeo prefixes
                sname = fileName.split('.')
                del sname[-1]
                del sname[-1]
                fileBase = '.'.join(sname)
                fileName = fileBase + ".clones.tab"
                fileOut = fileBase + ".clones.mutational." + key + ".tsv"

                with open(scriptName, 'w') as r_file:
                    r_file.write('library(ggplot2)\n')
                    r_file.write('library(alakazam)\n')
                    r_file.write('library(shazam)\n')
                    r_file.write('db <- readChangeoDb("' + fileName + '")\n')
                    r_file.write("db.a <- calcBaseline(db, testStatistic='local', sequenceColumn='SEQUENCE_IMGT', germlineColumn='GERMLINE_IMGT_D_MASK', regionDefinition=IMGT_V_BY_REGIONS, nproc=1)\n")
                    r_file.write("group.a <- groupBaseline(db.a, groupBy=c('FUNCTIONAL'))\n")
                    r_file.write("db.v <- calcBaseline(db, testStatistic='local', sequenceColumn='SEQUENCE_IMGT', germlineColumn='GERMLINE_IMGT_D_MASK', regionDefinition=IMGT_V, nproc=1)\n")
                    r_file.write("group.v <- groupBaseline(db.v, groupBy=c('FUNCTIONAL'))\n")
                    r_file.write("group.stats <- rbind(group.v@stats, group.a@stats)\n")
                    r_file.write("write.table(group.stats, row.names=F, sep='\\t', file='" + fileOut + "')\n")
                    if config['groups'][key]['type'] == 'file':
                        # write out the collapsed clone info for files
                        r_file.write("db.out <- db.a@db\n")
                        #r_file.write("db.out['OBSERVED_CDR_R'] <- db.v@db['OBSERVED_CDR_R']\n")
                        #r_file.write("db.out['OBSERVED_CDR_S'] <- db.v@db['OBSERVED_CDR_S']\n")
                        #r_file.write("db.out['OBSERVED_FWR_R'] <- db.v@db['OBSERVED_FWR_R']\n")
                        #r_file.write("db.out['OBSERVED_FWR_S'] <- db.v@db['OBSERVED_FWR_S']\n")
                        #r_file.write("db.out['EXPECTED_CDR_R'] <- db.v@db['EXPECTED_CDR_R']\n")
                        #r_file.write("db.out['EXPECTED_CDR_S'] <- db.v@db['EXPECTED_CDR_S']\n")
                        #r_file.write("db.out['EXPECTED_FWR_R'] <- db.v@db['EXPECTED_FWR_R']\n")
                        #r_file.write("db.out['EXPECTED_FWR_S'] <- db.v@db['EXPECTED_FWR_S']\n")
                        r_file.write("writeChangeoDb(db.out, '" + fileBase + ".clones.collapse.tsv')\n")
                        addGroupFileEntry(metadata, "output", key, metadata['process']['appName'],
                                          "collapsed_clones", fileBase + ".clones.collapse.tsv.zip", fileBase + " Collapsed Clones", "tsv", fileUuid)
                    r_file.write("q()\n")
                    addGroupFileEntry(metadata, "output", key, "selection_pressure",
                                      "selection_pressure", fileOut, "Selection Pressure", "tsv", fileUuid)

    # save the process metadata
    with open(args.process_metadata, 'w') as f:
        json.dump(metadata, f, indent=2)

else:
    # invalid arguments
    parser.print_help()


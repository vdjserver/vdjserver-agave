#
# VDJServer repcalc Agave wrapper script
# for Lonestar5
# 

# Configuration settings

VDJML_VERSION=1.0.0
# Path is slightly different between ls5 and stampede
IGDATA="$WORK/../common/igblast-db/db"

# These get set by Agave

# input files
ProjectDirectory="${ProjectDirectory}"
JobFiles="${JobFiles}"
VDJMLFiles="${VDJMLFiles}"
SummaryFiles="${SummaryFiles}"
ChangeOFiles="${ChangeOFiles}"
StudyMetadata="${StudyMetadata}"

# application parameters
JobSelected=${JobSelected}
SecondaryInputsFlag=${SecondaryInputsFlag}
# input file metadata
VDJMLFilesMetadata="${VDJMLFilesMetadata}"
SummaryFilesMetadata="${SummaryFilesMetadata}"
ChangeOFilesMetadata="${ChangeOFilesMetadata}"

GeneSegmentFlag=${GeneSegmentFlag}
GeneSegmentLevels="${GeneSegmentLevels}"
GeneSegmentOperations="${GeneSegmentOperations}"
GeneSegmentFilters="${GeneSegmentFilters}"

CDR3Flag=${CDR3Flag}
CDR3Levels="${CDR3Levels}"
CDR3Operations="${CDR3Operations}"
CDR3Filters="${CDR3Filters}"

DiversityFlag=${DiversityFlag}
DiversityLevels="${DiversityLevels}"
DiversityOperations="${DiversityOperations}"
DiversityFilters="${DiversityFilters}"

ClonalFlag=${ClonalFlag}
ClonalOperations="${ClonalOperations}"
ClonalFilters="${ClonalFilters}"

MutationalFlag=${MutationalFlag}
MutationalOperations="${MutationalOperations}"
MutationalFilters="${MutationalFilters}"

# Agave info
AGAVE_JOB_ID=${AGAVE_JOB_ID}
AGAVE_JOB_NAME=${AGAVE_JOB_NAME}
AGAVE_LOG_NAME=${AGAVE_JOB_NAME}-${AGAVE_JOB_ID}

# ----------------------------------------------------------------------------
# unpack local executables
tar zxf binaries.tgz
tar zxf launcher.tar.gz

# modules
module load python
module load Rstats

REPCALC_EXE=repcalc
PYTHON=python

export PATH="$PWD/bin:${PATH}"
export PYTHONPATH=$PWD/lib/python2.7/site-packages:$PYTHONPATH
export CHANGEO_PYTHON=$PWD/lib/python3.5/site-packages
export R_LIBS=$WORK/production/R_libs

# bring in common functions
source ./repcalc_common.sh

# ----------------------------------------------------------------------------
# Launcher to use multicores on node
export LAUNCHER_DIR=$PWD/launcher
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins
export LAUNCHER_WORKDIR=$PWD
export LAUNCHER_RMI=SLURM
export LAUNCHER_LOW_PPN=1
export LAUNCHER_MID_PPN=8
export LAUNCHER_MAX_PPN=25
export LAUNCHER_PPN=1
export LAUNCHER_JOB_FILE=joblist
export LAUNCHER_SCHED=interleaved

# Start
printf "START at $(date)\n\n"

export JOB_ERROR=0

gather_secondary_inputs
print_parameters
print_versions
run_repcalc_workflow

# End
printf "DONE at $(date)\n\n"

# remove binaries before archiving 
rm -rf bin lib launcher

if [[ $JOB_ERROR -eq 1 ]]; then
    ${AGAVE_JOB_CALLBACK_FAILURE}
fi

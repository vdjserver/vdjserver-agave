#
SYSTEM=ls5
VER=1.0
VDJML_VERSION=1.0.0

# Copy all of the object files to the bundle directory
# and create a binaries.tgz
#
# For example:
# cd bundle
# cp $WORK/../common/launcher/launcher.tar.gz .

# Install VDJML locally:
# cp $WORK/vdjml/$VDJML_VERSION/out/VDJMLpy-$VDJML_VERSION.tar.gz .
# tar zxvf VDJMLpy-$VDJML_VERSION.tar.gz
# cd VDJMLpy-$VDJML_VERSION
# export PYTHONPATH=../lib/python2.7/site-packages:$PYTHONPATH
# python setup.py install --prefix=..

# Install repsum locally:
# cd repertoire-summarization
# export PYTHONPATH=../lib/python2.7/site-packages:$PYTHONPATH
# python setup.py install --prefix=..

# Install Change-O locally (python 3):
# bash
# module unload python
# module load gcc python3
# cd changeo
# export PYTHONPATH=../lib/python3.5/site-packages:$PYTHONPATH
# python3 setup.py install --prefix=..
# exit

# R tools are installed in production area:
# module load Rstats
# export R_LIBS=$WORK/production/R_libs
# R --no-save
# install.packages(alakazam, repos="http://cran.r-project.org")
# install.packages(shazam, repos="http://cran.r-project.org")
#

#
# tar zcvf binaries.tgz bin lib

# delete old working area in agave
files-delete /apps/repcalc/$VER/$SYSTEM

# upload files
files-mkdir -N $VER /apps/repcalc
files-mkdir -N $SYSTEM /apps/repcalc/$VER
files-mkdir -N test /apps/repcalc/$VER/$SYSTEM
files-upload -F bundle/binaries.tgz /apps/repcalc/$VER/$SYSTEM
files-upload -F bundle/launcher.tar.gz /apps/repcalc/$VER/$SYSTEM
files-upload -F ../common/repcalc_common.sh /apps/repcalc/$VER/$SYSTEM
files-upload -F ../common/repcalc_create_config.py /apps/repcalc/$VER/$SYSTEM
files-upload -F ../common/do_clones.sh /apps/repcalc/$VER/$SYSTEM
files-upload -F ../common/get_parameter.py /apps/repcalc/$VER/$SYSTEM
files-upload -F ../common/create_r_scripts.py /apps/repcalc/$VER/$SYSTEM
files-upload -F ../../../common/process_metadata.py /apps/repcalc/$VER/$SYSTEM
files-upload -F repcalc.sh /apps/repcalc/$VER/$SYSTEM
files-upload -F repcalc.json /apps/repcalc/$VER/$SYSTEM

files-upload -F test/test.sh /apps/repcalc/$VER/$SYSTEM/test

files-upload -F ../common/test/tcr /apps/repcalc/$VER/$SYSTEM/test
files-upload -F test/test-tcr.json /apps/repcalc/$VER/$SYSTEM/test/tcr

files-upload -F ../common/test/bcr /apps/repcalc/$VER/$SYSTEM/test
files-upload -F test/test-bcr.json /apps/repcalc/$VER/$SYSTEM/test/bcr
files-upload -F test/test-bcr4.json /apps/repcalc/$VER/$SYSTEM/test/bcr


files-list -L /apps/repcalc/$VER/$SYSTEM
if [ "$(files-list -L /apps/repcalc/$VER/$SYSTEM | wc -l)" -ne 12 ]; then
    echo "ERROR: Wrong number of file entries in app directory."
fi
files-list -L /apps/repcalc/$VER/$SYSTEM/test

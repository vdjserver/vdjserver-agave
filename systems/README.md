# JSON definitions for execution/storage systems for VDJServer

Note that almost all systems need authentication. With the recent
multi-factor authentication being implemented at TACC, using passwords
for some systems, like Lonestar5, is no longer possible. Instead
ssh-keys need to be used. For those systems, you need to place the
public key (~/.ssh/id_rsa.pub) and the private key (~/.ssh/id_rsa)
into the JSON system defintion. The public key is a single line and
can be inserted as such in the JSON. The private key extends over
multiple lines in the id_rsa file and needs to be manipulated when put
into the JSON. Specifically, those multiple lines need to put
completely on a single line, between the quotes (") in the JSON, and a
newline indicator (\n) put a separator between each line. Almost as if
that string was being printf() in C code to produce the exact contents
of id_rsa file.

*IMPORTANT: NEVER COMMIT FILES TO REPOSITORY WITH PASSWORDS OR SSH KEYS!*

## Execution systems

* ls5.tacc.utexas.edu: Lonestar 5
* vdj-exec-02.tacc.utexas.edu: vdj-exec-02 for small vdj_pipe jobs
* stampede2.tacc.utexas.edu: Stampede2
* stampede.tacc.utexas.edu: Stampede (OBSOLETE)
* lonestar.tacc.utexas.edu: Lonestar 4 (OBSOLETE)
* vdj-exec-01.tacc.utexas.edu: vdj-exec-01 for small vdj_pipe jobs (OBSOLETE)

## Storage systems

* data.vdjserver.org: Main storage system
* sftp.vdjserver.org: Older name that should be OBSOLETE.

`data.vdjserver.org` is the main storage system for VDJServer
files. Previously, the files where stored on our own file servers,
`rogers.corral.tacc.utexas.edu` and `bowie.corral.tacc.utexas.edu`,
with one being the primary and the other a replicate, but we've
expanded beyond the disk space available. Files are now stored on the
huge Corral network file system. However, Agave doesn't talk to Corral
directly, instead still uses `rogers` or `bowie` which has the network
drive mounted.

```
$ df -h
c3-dtn02.corral.tacc.utexas.edu:/gpfs/corral3/repl/projects/vdjZ   12P  7.1P  4.3P  63% /vdjZ
```

There are a number of important things to be set on `rogers` and
`bowie` for everything to work properly.

* `/vdjZ` has to be the top of the tree. Agave keeps the full path to
  the file as in internal part of the file UUID, so if the path
  changes, the files are not longer the same and cannot be accessed.

* The vdj account needs to be in same user id and group id as the vdj
  account on TACC. This is to insure that the files can be accessed
  and get the correct permissions.

## System monitors

These unfortunately don't work well for what we want, i.e. to know
when a system is down, thus they aren't being used anywhere.

* monitor.lonestar.json
* monitor.ls5.json
* monitor.data.json
* monitor.vdj-exec-01.json

## Personal execution systems

With the small development team for VDJServer, I tend to use the vdj
account for developing the agave apps. But here are some old notes on
creating a personal development system that I pulled from the old
wiki.

*NOTE: THESE INSTRUCTIONS ARE NOT UP-TO-DATE*

If you want to test your own development versions of the agave apps on
Lonestar, then you will need to deploy those apps onto your own
execution system for Lonestar. The main VDJ execution systems are
reserved for use by the VDJ user for running production jobs submitted
by the website, and submitting your own jobs may interfering. However,
creating is straight forward. Template JSON files are available in the
vdjserver-agave repository in the dev-systems directory. You will need
to edit that file and change some of the properties:

* id: Give your system a unique name that is easy to remember and will
  not conflict with any public names, e.g. scott-ls5-dev

* workDir: This is the path to your work directory. Log on to Lonestar
  and check $WORK for the path.

* scratchDir: This is the path to your scratch directory. Log on to
  Lonestar and check $SCRATCH for the path. Also in the JSON is a
  storage stanza asking for homeDir which we suggest you set to the
  scratch directory to avoid disk quota limits.

* username/password: There are two sections in the JSON that require
your username/password, the login and storage stanzas. Use your
Lonestar login username (which maybe different from your VDJServer
username) and your password. Agave stores your password securely so
nobody else can see it, but you should delete the password from the
JSON file once you are done.  The rest of the properties can be left
as is. Now you can create the system using that JSON file:

```
$ systems-addupdate -F mylonestar5.json
Successfully added system scott-ls5-dev
```

#
# Extract out FASTA sequences from Adaptive Biotech file
#
# Author: Scott Christley
# Date: May 23, 2017
#

from __future__ import print_function
import json
import argparse
import os
import sys

if (__name__=="__main__"):
    parser = argparse.ArgumentParser(description='Convert Adaptive Biotech export file.')
    parser.add_argument('--metadata', help='Sample metadata file')
    parser.add_argument('adaptive_file', nargs='*', type=str, help='Input Adaptive file names')
    args = parser.parse_args()

    if args:
        if args.metadata:
            mfile = open(args.metadata, 'w')
            mkeys = ['Name', 'project_file']
            mdicts = []

        for fname in args.adaptive_file:
            with open(fname, 'rU') as infile:
                print('Processing ' + fname)
                first = True
                mfirst = True
                outname = '.'.join(fname.split('.')[0:-1]) + '.fasta'
                outname = outname.replace(' ', '_')
                outfile = open(outname, 'w')
                count = 0
                counting_method = -1
                while True:
                    line = infile.readline().rstrip('\n')
                    if not line: break
                    fields = line.split('\t')
                    if first:
                        first = False
                        counting_method = fields.index('counting_method')
                        if counting_method < 0:
                            print('ERROR: cannot find counting_method column.')
                            sys.exit(1)
                        sample_tags = fields.index('sample_tags')
                        sample_name = fields.index('sample_name')
                        continue

                    if mfirst and args.metadata:
                        mdict = { 'Name': fields[sample_name], 'project_file': outname.split('/')[-1] }
                        mdicts.append(mdict)
                        sample_fields = fields[sample_tags].replace('"', '').split(',')
                        sample_keys = []
                        for tag in sample_fields:
                            sample_values = tag.split(':')
                            test_name = sample_values[0]
                            kcnt = 0
                            while test_name in sample_keys:
                                kcnt += 1
                                test_name = sample_values[0] + ' (' + str(kcnt) + ')'
                            if test_name not in mkeys: mkeys.append(test_name)
                            sample_keys.append(test_name)
                            print(sample_values)
                            mdict[test_name] = sample_values[1]
                        mfirst = False

                    if fields[counting_method] == 'v1':
                        outfile.write('>' + '.'.join(outname.split('.')[0:-1]) + '.seq.' + str(count) + '|DUPCOUNT=' + str(fields[5]) + '\n')
                        outfile.write(fields[0] + '\n')
                    if fields[counting_method] == 'v2':
                        outfile.write('>' + '.'.join(outname.split('.')[0:-1]) + '.seq.' + str(count) + '|DUPCOUNT=' + str(fields[4]) + '\n')
                        outfile.write(fields[0] + '\n')
                    if fields[counting_method] == 'v3':
                        outfile.write('>' + '.'.join(outname.split('.')[0:-1]) + '.seq.' + str(count) + '|DUPCOUNT=' + str(fields[4]) + '\n')
                        outfile.write(fields[0] + '\n')
                    count += 1
            outfile.close()

        if args.metadata:
            mfile.write('\t'.join(mkeys) + '\n')
            for mdict in mdicts:
                first = True
                for mkey in mkeys:
                    if first: first = False
                    else: mfile.write('\t')
                    if mdict.get(mkey): mfile.write(mdict[mkey])
                mfile.write('\n')
            mfile.close()

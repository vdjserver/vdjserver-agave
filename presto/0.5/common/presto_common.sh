#
# VDJServer pRESTO common functions
#
# This script relies upon global variables
# source presto_common.sh
#
# Author: Scott Christley
# Date: August 11, 2016
# 

# ----------------------------------------------------------------------------
function expandfile () {
    fileBasename="${1%.*}" # file.txt.gz -> file.txt
    fileExtension="${1##*.}" # file.txt.gz -> gz

    if [ ! -f $1 ]; then
        echo "Could not find query input file $1" 1>&2
        exit 1
    fi

    if [ "$fileExtension" == "gz" ]; then
        gunzip $1
        export file=$fileBasename
        # don't archive the intermediate file
    elif [ "$fileExtension" == "bz2" ]; then
        bunzip2 $1
        export file=$fileBasename
    elif [ "$fileExtension" == "zip" ]; then
        unzip $1
        export file=$fileBasename
    else
        export file=$1
    fi

}

function noArchive() {
    echo $1 >> .agave.archive
}

# ----------------------------------------------------------------------------
# Process workflow metadata
APP_NAME=presto

function initProcessMetadata() {
    $PYTHON3 ./process_metadata.py --init $APP_NAME $AGAVE_JOB_ID process_metadata.json
    # collect all output files
    mkdir ${AGAVE_JOB_ID}
    noArchive ${AGAVE_JOB_ID}
    ARCHIVE_FILE_LIST=""
}

function addStatisticsFile() {
    $PYTHON3 ./process_metadata.py --entry statistics "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
}

function addLogFile() {
    $PYTHON3 ./process_metadata.py --entry log "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
}

function addOutputFile() {
    $PYTHON3 ./process_metadata.py --entry output "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
    ARCHIVE_FILE_LIST="${ARCHIVE_FILE_LIST} $4"
}

function addGroup() {
    $PYTHON3 ./process_metadata.py --group "$1" "$2" process_metadata.json
}

function addCalculation() {
    $PYTHON3 ./process_metadata.py --calc $1 process_metadata.json
}

# ----------------------------------------------------------------------------
function gather_secondary_inputs() {
    # Gather secondary input files
    # This is used to get around Agave size limits for job inputs and parameters
    if [[ $SecondaryInputsFlag -eq 1 ]]; then
	echo "Gathering secondary input"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" SequenceFilesMetadata study_metadata.json)
	SequenceFiles="${SequenceFiles} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryEntry SequenceFilesMetadata study_metadata.json)
	SequenceFilesMetadata="${SequenceFilesMetadata} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" SequenceForwardPairedFilesMetadata study_metadata.json)
	SequenceForwardPairedFiles="${SequenceForwardPairedFiles} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryEntry SequenceForwardPairedFilesMetadata study_metadata.json)
	SequenceForwardPairedFilesMetadata="${SequenceForwardPairedFilesMetadata} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" SequenceReversePairedFilesMetadata study_metadata.json)
	SequenceReversePairedFiles="${SequenceReversePairedFiles} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryEntry SequenceReversePairedFilesMetadata study_metadata.json)
	SequenceReversePairedFilesMetadata="${SequenceReversePairedFilesMetadata} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" ForwardPrimerFileMetadata study_metadata.json)
	ForwardPrimerFile="${ForwardPrimerFile} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryEntry ForwardPrimerFileMetadata study_metadata.json)
	ForwardPrimerFileMetadata="${ForwardPrimerFileMetadata} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" ReversePrimerFileMetadata study_metadata.json)
	ReversePrimerFile="${ReversePrimerFile} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryEntry ReversePrimerFileMetadata study_metadata.json)
	ReversePrimerFileMetadata="${ReversePrimerFileMetadata} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" BarcodeFileMetadata study_metadata.json)
	BarcodeFile="${BarcodeFile} ${moreFiles}"
	moreFiles=$(${PYTHON3} ./process_metadata.py --getSecondaryEntry BarcodeFileMetadata study_metadata.json)
	BarcodeFileMetadata="${BarcodeFileMetadata} ${moreFiles}"
    fi
}

function print_versions() {
    # Start
    echo "VERSIONS:"
    echo "  $($ALIGN_SETS_PY --version 2>&1)"
    echo "  $($ASSEMBLE_PAIRS_PY --version 2>&1)"
    echo "  $($BUILD_CONSENSUS_PY --version 2>&1)"
    echo "  $($CLUSTER_SETS_PY --version 2>&1)"
    echo "  $($COLLAPSE_SEQ_PY --version 2>&1)"
    echo "  $($CONVERT_HEADERS_PY --version 2>&1)"
    echo "  $($FILTER_SEQ_PY --version 2>&1)"
    echo "  $($MASK_PRIMERS_PY --version 2>&1)"
    echo "  $($PAIR_SEQ_PY --version 2>&1)"
    echo "  $($PARSE_HEADERS_PY --version 2>&1)"
    echo "  $($PARSE_LOG_PY --version 2>&1)"
    echo "  $($SPLIT_SEQ_PY --version 2>&1)"
    echo "  $($VDJ_PIPE --version 2>&1)"
    echo -e "\nSTART at $(date)"
}

function print_parameters() {
    echo "Input files:"
    echo "ProjectDirectory=${ProjectDirectory}"
    echo "JobFiles=$JobFiles"
    echo "SequenceFiles=$SequenceFiles"
    echo "SequenceForwardPairedFiles=$SequenceForwardPairedFiles"
    echo "SequenceReversePairedFiles=$SequenceReversePairedFiles"
    echo "ForwardPrimerFile=$ForwardPrimerFile"
    echo "ReversePrimerFile=$ReversePrimerFile"
    echo "BarcodeFile=$BarcodeFile"
    echo ""
    echo "Application parameters:"
    echo "Workflow=$Workflow"
    echo "SecondaryInputsFlag=${SecondaryInputsFlag}"
    echo "SequenceFileTypes=$SequenceFileTypes"
    echo "Input file metadata:"
    echo "SequenceFilesMetadata=${SequenceFilesMetadata}"
    echo "SequenceForwardPairedFilesMetadata=${SequenceForwardPairedFilesMetadata}"
    echo "SequenceReversePairedFilesMetadata=${SequenceReversePairedFilesMetadata}"
    echo "ForwardPrimerFileMetadata=${ForwardPrimerFileMetadata}"
    echo "ReversePrimerFileMetadata=${ReversePrimerFileMetadata}"
    echo "BarcodeFileMetadata=${BarcodeFileMetadata}"
    echo "Pre-filter statistics:"
    echo "PreFilterStatisticsFlag=$PreFilterStatisticsFlag"
    echo "Filters:"
    echo "FilterFlag=$FilterFlag"
    echo "MinimumQuality=$MinimumQuality"
    echo "MinimumLength=$MinimumLength"
    echo "Post-filter statistics:"
    echo "PostFilterStatisticsFlag=$PostFilterStatisticsFlag"
    echo "Barcodes:"
    echo "Barcode=$Barcode"
    echo "BarcodeMaxError=$BarcodeMaxError"
    echo "BarcodeStartPosition=$BarcodeStartPosition"
    echo "BarcodeSplitFlag=$BarcodeSplitFlag"
    echo "UMI:"
    echo "UMIConsensus=$UMIConsensus"
    echo "UMIMaxError=$UMIMaxError"
    echo "UMIMaxGap=$UMIMaxGap"
    echo "UMIMinFrequency=$UMIMinFrequency"
    echo "Forward primer:"
    echo "ForwardPrimer=$ForwardPrimer"
    echo "ForwardPrimerUMI=$ForwardPrimerUMI"
    echo "ForwardPrimerMaxError=$ForwardPrimerMaxError"
    echo "ForwardPrimerMaxLength=$ForwardPrimerMaxLength"
    echo "ForwardPrimerStartPosition=$ForwardPrimerStartPosition"
    echo "Reverse primer:"
    echo "ReversePrimer=$ReversePrimer"
    echo "ReversePrimerUMI=$ReversePrimerUMI"
    echo "ReversePrimerMaxError=$ReversePrimerMaxError"
    echo "ReversePrimerMaxLength=$ReversePrimerMaxLength"
    echo "ReversePrimerStartPosition=$ReversePrimerStartPosition"
    echo "Find unique sequences:"
    echo "FindUniqueFlag=$FindUniqueFlag"
    echo "FindUniqueMaxNucleotides=$FindUniqueMaxNucleotides"
    echo "FindUniqueExclude=$FindUniqueExclude"
}

function run_presto_workflow() {
    intermediateFiles=()
    initProcessMetadata
    addLogFile $APP_NAME log stdout "${AGAVE_LOG_NAME}.out" "Job Output Log" "log" null
    addLogFile $APP_NAME log stderr "${AGAVE_LOG_NAME}.err" "Job Error Log" "log" null
    addLogFile $APP_NAME log agave_log .agave.log "Agave Output Log" "log" null

    # Exclude input files from archive
    noArchive "${ProjectDirectory}"
    for file in $JobFiles; do
	if [ -f $file ]; then
	    unzip $file
	    noArchive $file
	    noArchive "${file%.*}"
	fi
    done

    # Assemble paired reads
    if [ "$Workflow" = "paired" ]; then
	echo "Convert and assemble paired reads"
	forwardReads=($SequenceForwardPairedFiles)
	forwardReadsMetadata=($SequenceForwardPairedFilesMetadata)
	reverseReads=($SequenceReversePairedFiles)

	count=0
	while [ "x${forwardReads[count]}" != "x" ]
	do
	    group="merge${count}"
	    addGroup $group file

            # uncompress if needed
	    file=${reverseReads[count]}
	    mfile=${forwardReadsMetadata[count]}
            expandfile $file
	    noArchive $file

	    # presto needs extension to be fastq
	    testExt="${file##*.}" # file.fq -> fq
	    if [ "$testExt" != "fastq" ]; then
		echo "Warning: Renaming file to have fastq extension"
		cp $file ${file}.fastq
		file=${file}.fastq
		noArchive "$file"
	    fi
	    rfile=$file

	    file=${forwardReads[count]}
            expandfile $file
	    noArchive $file

	    # presto needs extension to be fastq
	    testExt="${file##*.}" # file.fq -> fq
	    if [ "$testExt" != "fastq" ]; then
		echo "Warning: Renaming file to have fastq extension"
		cp $file ${file}.fastq
		file=${file}.fastq
		noArchive "$file"
	    fi

	    fileExtension="${file##*.}" # file.fastq -> fastq
	    fileBasename="${file%.*}" # file.fastq -> file
	    fileOutname="${fileBasename##*/}" # test/file -> file
	    filePrefix="${file%/*}" # test/file -> file, or file -> file
	    if [ "$filePrefix" == "$file" ]; then
		# no directory in filename
		filePrefix=""
	    fi

            # attach prefix
	    OutputPrefix=$fileOutname
	    if [ -n "${filePrefix}" ]; then
		OutputName="${filePrefix}/${fileOutname}"
	    else
		OutputName="${fileOutname}"
	    fi

	    ARGS="align -1 $file -2 $rfile --coord $SequenceFileTypes --rc tail"
	    ARGS="${ARGS} --outname $OutputPrefix"
	    echo AssemblePairs.py $ARGS
	    $ASSEMBLE_PAIRS_PY $ARGS
	    addCalculation merge_paired_reads

	    SequenceFiles="$SequenceFiles ${OutputName}_assemble-pass.${fileExtension}"
	    SequenceFilesMetadata="$SequenceFilesMetadata $group"
	    cp ${OutputName}_assemble-pass.${fileExtension} ${OutputPrefix}_assemble.${fileExtension}
	    addOutputFile $group $APP_NAME merged_sequence "${OutputPrefix}_assemble.${fileExtension}" "Merged Pre-Filter Sequences (${OutputPrefix})" "read" "$mfile"

	    count=$(( $count + 1 ))
	done
	
    fi

    readFiles=($SequenceFiles)
    readFilesMetadata=($SequenceFilesMetadata)

    count=0
    while [ "x${readFiles[count]}" != "x" ]
    do
	file=${readFiles[count]}
	mfile=${readFilesMetadata[count]}

	group="group${count}"
	addGroup $group file

	# uncompress if needed
	expandfile $file
	noArchive $file

	# presto needs extension to be fastq
	testExt="${file##*.}" # file.fq -> fq
	if [ "$testExt" != "fastq" ]; then
	    echo "Warning: Renaming file to have fastq extension"
	    cp $file ${file}.fastq
	    file=${file}.fastq
	    noArchive "$file"
	fi

	fileExtension="${file##*.}" # file.fastq -> fastq
	fileBasename="${file%.*}" # file.fastq -> file
	fileOutname="${fileBasename##*/}" # test/file -> file
	filePrefix="${file%/*}" # test/file -> file, or file -> file
	if [ "$filePrefix" == "$file" ]; then
	    # no directory in filename
	    filePrefix=""
	fi

        # attach prefix
	OutputPrefix=$fileOutname
	if [ -n "${filePrefix}" ]; then
	    OutputName="${filePrefix}/${fileOutname}"
	else
	    OutputName="${fileOutname}"
	fi

	# Run vdjpipe to generate statistics
	if [[ $PreFilterStatisticsFlag -eq 1 ]]; then
	    echo "Generate pre-filter statistics"

	    $PYTHON3 ./statistics.py statistics-template.json $file "pre-filter_" "pre-statistics.json"
	    addStatisticsFile $group pre composition "pre-filter_${file}.composition.csv" "Nucleotide Composition" "tsv" $mfile
	    addStatisticsFile $group pre gc_hist "pre-filter_${file}.gc_hist.csv" "GC% Histogram" "tsv" $mfile
	    addStatisticsFile $group pre heat_map "pre-filter_${file}.heat_map.csv" "Heatmap" "tsv" $mfile
	    addStatisticsFile $group pre len_hist "pre-filter_${file}.len_hist.csv" "Sequence Length Histogram" "tsv" $mfile
	    addStatisticsFile $group pre mean_q_hist "pre-filter_${file}.mean_q_hist.csv" "Mean Quality Histogram" "tsv" $mfile
	    addStatisticsFile $group pre qstats "pre-filter_${file}.qstats.csv" "Quality Scores" "tsv" $mfile

	    noArchive "pre-statistics.json"
	    noArchive "summary.txt"
	    $VDJ_PIPE --config pre-statistics.json
	    addCalculation "pre-filter_statistics"
	fi

	prevPassFile=$file

	# Filter sequences for quality and length
	if [[ $FilterFlag -eq 1 ]]; then
	    ARGS="length"
	    if [ -n "$MinimumLength" ]; then
		ARGS="${ARGS} -n $MinimumLength"
	    fi
	    ARGS="${ARGS} --outname $OutputPrefix"
	    ARGS="${ARGS} -s $prevPassFile"
	    echo FilterSeq.py $ARGS
	    $FILTER_SEQ_PY $ARGS
	    addCalculation length_filtering

	    noArchive "$prevPassFile"
	    intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    prevPassFile="${OutputName}_length-pass.${fileExtension}"

	    ARGS="quality"
	    if [ -n "$MinimumQuality" ]; then
		ARGS="${ARGS} -q $MinimumQuality"
	    fi
	    ARGS="${ARGS} --outname $OutputPrefix"
	    ARGS="${ARGS} -s $prevPassFile"
	    echo FilterSeq.py $ARGS
	    $FILTER_SEQ_PY $ARGS
	    addCalculation quality_filtering

	    noArchive "$prevPassFile"
	    intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    prevPassFile="${OutputName}_quality-pass.${fileExtension}"
	fi

	# Run vdjpipe to generate statistics
	if [[ $PostFilterStatisticsFlag -eq 1 ]]; then
	    echo "Generate post-filter statistics"

	    $PYTHON3 ./statistics.py statistics-template.json $prevPassFile "post-filter_" "post-statistics.json"
	    addStatisticsFile $group post composition "post-filter_${prevPassFile}.composition.csv" "Nucleotide Composition" "tsv" $mfile
	    addStatisticsFile $group post gc_hist "post-filter_${prevPassFile}.gc_hist.csv" "GC% Histogram" "tsv" $mfile
	    addStatisticsFile $group post heat_map "post-filter_${prevPassFile}.heat_map.csv" "Heatmap" "tsv" $mfile
	    addStatisticsFile $group post len_hist "post-filter_${prevPassFile}.len_hist.csv" "Sequence Length Histogram" "tsv" $mfile
	    addStatisticsFile $group post mean_q_hist "post-filter_${prevPassFile}.mean_q_hist.csv" "Mean Quality Histogram" "tsv" $mfile
	    addStatisticsFile $group post qstats "post-filter_${prevPassFile}.qstats.csv" "Quality Scores" "tsv" $mfile

	    noArchive "post-statistics.json"
	    noArchive "summary.txt"
	    $VDJ_PIPE --config post-statistics.json
	    addCalculation "post-filter_statistics"
	fi

	EXPAND_PRIMER=

	# Barcode
	if [[ $Barcode -eq 1 ]]; then
	    echo "Processing barcodes"
	    if [ -z "$BarcodeFile" ]; then
		echo "ERROR: Missing the required Barcode file."
		exit
	    fi
	    noArchive "$BarcodeFile"

	    # presto needs extension to be fasta
	    testExt="${BarcodeFile##*.}" # file.fasta -> fasta
	    if [ "$testExt" != "fasta" ]; then
		echo "Warning: Renaming barcode file to have fasta extension"
		cp $BarcodeFile ${BarcodeFile}.fasta
		BarcodeFile=${BarcodeFile}.fasta
		noArchive "$BarcodeFile"
	    fi

	    ARGS="score -p $BarcodeFile -s $prevPassFile"
	    if [ -n "$BarcodeMaxError" ]; then
		ARGS="${ARGS} --maxerror $BarcodeMaxError"
	    fi
	    if [ -n "$BarcodeStartPosition" ]; then
		ARGS="${ARGS} --start $BarcodeStartPosition"
	    fi
	    ARGS="${ARGS} --outname ${OutputPrefix}-barcode --mode cut"
	    echo MaskPrimers.py $ARGS
	    $MASK_PRIMERS_PY $ARGS
	    addCalculation barcode_demultiplexing

	    noArchive "$prevPassFile"
	    intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    prevPassFile="${OutputName}-barcode_primers-pass.${fileExtension}"
	    EXPAND_PRIMER=1
	fi

	# Forward Primers
	if [ -n "$ForwardPrimer" ]; then
	    if [ "$ForwardPrimer" != "none" ]; then
		echo "Mask forward primers"
		if [ -z "$ForwardPrimerFile" ]; then
		    echo "ERROR: Missing the required forward primer file."
		    exit
		fi
		noArchive "$ForwardPrimerFile"

	        # presto needs extension to be fasta
		testExt="${ForwardPrimerFile##*.}" # file.fasta -> fasta
		if [ "$testExt" != "fasta" ]; then
		    echo "Warning: Renaming forward primer file to have fasta extension"
		    cp $ForwardPrimerFile ${ForwardPrimerFile}.fasta
		    ForwardPrimerFile=${ForwardPrimerFile}.fasta
		    noArchive "$ForwardPrimerFile"
		fi

		if [ "$ForwardPrimer" = "score" ]; then
		    ARGS="score -p $ForwardPrimerFile -s $prevPassFile"
		    if [ -n "$ForwardPrimerStartPosition" ]; then
			ARGS="${ARGS} --start $ForwardPrimerStartPosition"
		    fi
		    if [ -n "$ForwardPrimerMaxError" ]; then
			ARGS="${ARGS} --maxerror $ForwardPrimerMaxError"
		    fi
		else
		    ARGS="align -p $ForwardPrimerFile -s $prevPassFile"
		    if [ -n "$ForwardPrimerMaxError" ]; then
			ARGS="${ARGS} --maxerror $ForwardPrimerMaxError"
		    fi
		    if [ -n "$ForwardPrimerMaxLength" ]; then
			ARGS="${ARGS} --maxlen $ForwardPrimerMaxLength"
		    fi
		fi
		if [[ $ForwardPrimerUMI -eq 1 ]]; then
		    ARGS="${ARGS} --barcode"
		fi
		ARGS="${ARGS} --outname ${OutputPrefix}-V --mode mask"
		echo MaskPrimers.py $ARGS
		$MASK_PRIMERS_PY $ARGS
		addCalculation forward_primer

		noArchive "$prevPassFile"
		intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
		prevPassFile="${OutputName}-V_primers-pass.${fileExtension}"
		EXPAND_PRIMER=1
	    fi
	fi

	# Reverse Primers
	if [ -n "$ReversePrimer" ]; then
	    if [ "$ReversePrimer" != "none" ]; then
		echo "Mask reverse primers"
		if [ -z "$ReversePrimerFile" ]; then
		    echo "ERROR: Missing the required reverse primer file."
		    exit
		fi
		noArchive "$ReversePrimerFile"

	        # presto needs extension to be fasta
		testExt="${ReversePrimerFile##*.}" # file.fasta -> fasta
		if [ "$testExt" != "fasta" ]; then
		    echo "Warning: Renaming reverse primer file to have fasta extension"
		    cp $ReversePrimerFile ${ReversePrimerFile}.fasta
		    ReversePrimerFile=${ReversePrimerFile}.fasta
		    noArchive "$ReversePrimerFile"
		fi

		ARGS=""
		if [ "$ReversePrimer" = "score" ]; then
		    ARGS="score -p $ReversePrimerFile -s $prevPassFile"
		    if [ -n "$ReversePrimerStartPosition" ]; then
			ARGS="${ARGS} --start $ReversePrimerStartPosition"
		    fi
		    if [ -n "$ReversePrimerMaxError" ]; then
			ARGS="${ARGS} --maxerror $ReversePrimerMaxError"
		    fi
		    ARGS="${ARGS} --mode cut --revpr"
		else
		    ARGS="align -p $ReversePrimerFile -s $prevPassFile"
		    if [ -n "$ReversePrimerMaxError" ]; then
			ARGS="${ARGS} --maxerror $ReversePrimerMaxError"
		    fi
		    if [ -n "$ReversePrimerMaxLength" ]; then
			ARGS="${ARGS} --maxlen $ReversePrimerMaxLength"
		    fi
		    ARGS="${ARGS} --mode cut --revpr --skiprc"
		fi
		if [[ $ReversePrimerUMI -eq 1 ]]; then
		    ARGS="${ARGS} --barcode"
		fi
		ARGS="${ARGS} --outname ${OutputPrefix}-J"
		echo MaskPrimers.py $ARGS
		$MASK_PRIMERS_PY $ARGS
		addCalculation reverse_primer

		noArchive "$prevPassFile"
		intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
		prevPassFile="${OutputName}-J_primers-pass.${fileExtension}"
		EXPAND_PRIMER=1
	    fi
	fi

	if [[ $UMIConsensus -eq 1 ]]; then
	    echo "Generating UMI consensus reads"
	    ARGS="-s $prevPassFile --bf BARCODE --pf PRIMER"
	    if [ -n "$UMIMaxError" ]; then
		ARGS="${ARGS} --maxerror $UMIMaxError"
	    fi
	    if [ -n "$UMIMaxGap" ]; then
		ARGS="${ARGS} --maxgap $UMIMaxGap"
	    fi
	    if [ -n "$UMIMinFrequency" ]; then
		ARGS="${ARGS} --prcons $UMIMinFrequency"
	    fi
	    ARGS="${ARGS} --outname ${OutputPrefix}-UMI"

	    echo BuildConsensus.py $ARGS
	    $BUILD_CONSENSUS_PY $ARGS
	    addCalculation umi_consensus

	    noArchive "$prevPassFile"
	    intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    prevPassFile="${OutputName}-UMI_consensus-pass.${fileExtension}"

	    EXPAND_PRIMER=0
	    unique_fields=PRCONS
	    copy_fields=CONSCOUNT
	fi

	# Expand and rename primer field
	if [[ $EXPAND_PRIMER -eq 1 ]]; then
	    $PARSE_HEADERS_PY expand -s $prevPassFile --outname ${OutputPrefix}-expand -f PRIMER

	    noArchive "$prevPassFile"
	    intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    prevPassFile="${OutputName}-expand_reheader.${fileExtension}"

	    old_num=1
	    old_name=
	    new_name=
	    unique_fields=
	    copy_fields=
	    if [[ $Barcode -eq 1 ]]; then
		old_name="${old_name} PRIMER${old_num}"
		let "old_num+=1"
		new_name="${new_name} MID"
		unique_fields="${unique_fields} MID"
	    fi
	    if [ "$ForwardPrimer" != "none" ]; then
		old_name="${old_name} PRIMER${old_num}"
		let "old_num+=1"
		new_name="${new_name} VPRIMER"
		copy_fields="${copy_fields} VPRIMER"
	    fi
	    if [ "$ReversePrimer" != "none" ]; then
		old_name="${old_name} PRIMER${old_num}"
		let "old_num+=1"
		new_name="${new_name} JPRIMER"
		unique_fields="${unique_fields} JPRIMER"
	    fi

	    echo ParseHeaders.py rename -s $prevPassFile --outname ${OutputPrefix}-rename -f $old_name -k $new_name
	    $PARSE_HEADERS_PY rename -s $prevPassFile --outname ${OutputPrefix}-rename -f $old_name -k $new_name

	    noArchive "$prevPassFile"
	    intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    prevPassFile="${OutputName}-rename_reheader.${fileExtension}"
	fi

	# Rename file output file
	noArchive "$prevPassFile"
	intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	cp $prevPassFile ${OutputPrefix}-final.${fileExtension}
	prevPassFile="${OutputPrefix}-final.${fileExtension}"
	addOutputFile $group $APP_NAME processed_sequence "$prevPassFile" "Total Post-Filter Sequences (${OutputPrefix})" "read" $mfile

	# Split by barcode
	if [[ $BarcodeSplitFlag -eq 1 ]]; then
	    echo "Split by barcode"
	    $SPLIT_SEQ_PY group -s $prevPassFile -f MID
	fi

	# Find unique sequences
	if [[ $FindUniqueFlag -eq 1 ]]; then
	    echo "Find unique sequences"
	    if [[ $UMIConsensus -eq 1 ]]; then
		echo ParseHeaders.py collapse -s $prevPassFile -f CONSCOUNT --act min --outname ${OutputPrefix}
		$PARSE_HEADERS_PY collapse -s $prevPassFile -f CONSCOUNT --act min --outname ${OutputPrefix}

		prevPassFile="${OutputName}_reheader.${fileExtension}"
		noArchive "$prevPassFile"
		intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile
	    fi

	    ARGS="-s $prevPassFile"
	    if [ -n "$FindUniqueMaxNucleotides" ]; then
		ARGS="${ARGS} -n $FindUniqueMaxNucleotides"
	    fi
	    if [ -n "$FindUniqueExclude" ]; then
		ARGS="${ARGS} --inner"
	    fi
	    if [ -n "$unique_fields" ]; then
		ARGS="${ARGS} --uf $unique_fields"
	    fi
	    if [ -n "$copy_fields" ]; then
		ARGS="${ARGS} --cf $copy_fields"
		if [[ $UMIConsensus -eq 1 ]]; then
		    ARGS="${ARGS} --act sum"
		else
		    ARGS="${ARGS} --act set"
		fi
	    fi
	    ARGS="${ARGS} --outname ${OutputPrefix}"
	    echo CollapseSeq.py $ARGS
	    $COLLAPSE_SEQ_PY $ARGS
	    addCalculation find_unique_sequences

	    prevPassFile="${OutputPrefix}_collapse-unique.${fileExtension}"
	    addOutputFile $group $APP_NAME sequence "$prevPassFile" "Unique Post-Filter Sequences (${OutputPrefix})" "read" $mfile

	    # don't exclude DUPCOUNT=1 sequences
	    #noArchive "$prevPassFile"
	    #intermediateFiles[${#intermediateFiles[@]}]=$prevPassFile

	    #$SPLIT_SEQ_PY group -s $prevPassFile -f DUPCOUNT --num 2 --outname "${OutputPrefix}"
	    #prevPassFile="${OutputPrefix}_atleast-2.${fileExtension}"
	    #addOutputFile $group $APP_NAME singletons "${OutputPrefix}_under-2.${fileExtension}" "Singleton Post-Filter Sequences (${OutputPrefix})" "read" $mfile
	    #addOutputFile $group $APP_NAME sequence "$prevPassFile" "Unique Post-Filter Sequences (${OutputPrefix})" "read" $mfile

            # Split by barcode
	    if [[ $BarcodeSplitFlag -eq 1 ]]; then
		echo "Split by barcode"
		$SPLIT_SEQ_PY group -s $prevPassFile -f MID
	    fi
	fi

	# archive the intermediate files
	echo "Zip archive the intermediate files"
	for file in ${intermediateFiles[@]}; do
	    zip ${OutputPrefix}_intermediateFiles.zip $file
	done
	addLogFile $group $APP_NAME intermediate "${OutputPrefix}_intermediateFiles.zip" "Intermediate Files (${OutputPrefix})" "zip" $mfile

	count=$(( $count + 1 ))
    done

    # zip archive of all output files
    for file in $ARCHIVE_FILE_LIST; do
	if [ -f $file ]; then
	    cp -f $file ${AGAVE_JOB_ID}
	fi
    done
    zip ${AGAVE_JOB_ID}.zip ${AGAVE_JOB_ID}/*
    addLogFile $APP_NAME log output_archive ${AGAVE_JOB_ID}.zip "Archive of Output Files" "zip" null
}

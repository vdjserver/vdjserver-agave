#
# VDJServer pRESTO Agave wrapper script
# for Lonestar5
# 

# debug
# set +x

# These get set by Agave

# input files
ProjectDirectory="${ProjectDirectory}"
JobFiles="${JobFiles}"
SequenceFiles="${SequenceFiles}"
SequenceForwardPairedFiles="${SequenceForwardPairedFiles}"
SequenceReversePairedFiles="${SequenceReversePairedFiles}"
ForwardPrimerFile="${ForwardPrimerFile}"
ReversePrimerFile="${ReversePrimerFile}"
BarcodeFile="${BarcodeFile}"

# application parameters
Workflow=${Workflow}
SecondaryInputsFlag=${SecondaryInputsFlag}
SequenceFileTypes="${SequenceFileTypes}"
# input file metadata
SequenceFilesMetadata="${SequenceFilesMetadata}"
SequenceForwardPairedFilesMetadata="${SequenceForwardPairedFilesMetadata}"
SequenceReversePairedFilesMetadata="${SequenceReversePairedFilesMetadata}"
ForwardPrimerFileMetadata="${ForwardPrimerFileMetadata}"
ReversePrimerFileMetadata="${ReversePrimerFileMetadata}"
BarcodeFileMetadata="${BarcodeFileMetadata}"
# pre-filter statistics
PreFilterStatisticsFlag=${PreFilterStatisticsFlag}
# FilterSeq
FilterFlag=${FilterFlag}
MinimumQuality=${MinimumQuality}
MinimumLength=${MinimumLength}
# post-filter statistics
PostFilterStatisticsFlag=${PostFilterStatisticsFlag}
# Barcodes
Barcode=${Barcode}
BarcodeMaxError=${BarcodeMaxError}
BarcodeStartPosition=${BarcodeStartPosition}
BarcodeSplitFlag=${BarcodeSplitFlag}
# UMI
UMIConsensus=${UMIConsensus}
UMIMaxError=${UMIMaxError}
UMIMaxGap=${UMIMaxGap}
UMIMinFrequency=${UMIMinFrequency}
# V primer
ForwardPrimer=${ForwardPrimer}
ForwardPrimerUMI=${ForwardPrimerUMI}
ForwardPrimerMaxError=${ForwardPrimerMaxError}
ForwardPrimerMaxLength=${ForwardPrimerMaxLength}
ForwardPrimerStartPosition=${ForwardPrimerStartPosition}
# J primer
ReversePrimer=${ReversePrimer}
ReversePrimerUMI=${ReversePrimerUMI}
ReversePrimerMaxError=${ReversePrimerMaxError}
ReversePrimerMaxLength=${ReversePrimerMaxLength}
ReversePrimerStartPosition=${ReversePrimerStartPosition}
# Find unique sequences
FindUniqueFlag=${FindUniqueFlag}
FindUniqueMaxNucleotides=${FindUniqueMaxNucleotides}
FindUniqueExclude=${FindUniqueExclude}

# Agave info
AGAVE_JOB_ID=${AGAVE_JOB_ID}
AGAVE_JOB_NAME="${AGAVE_JOB_NAME}"
AGAVE_LOG_NAME=${AGAVE_JOB_NAME}-${AGAVE_JOB_ID}

# ----------------------------------------------------------------------------
tar xzf binaries.tgz

# modules
module unload python
module -q load gcc python3

export PATH="$PWD/bin:${PATH}"
export PYTHONPATH=$PWD/lib/python3.5/site-packages:$PYTHONPATH

# use our production binaries
ALIGN_SETS_PY="AlignSets.py"
ASSEMBLE_PAIRS_PY="AssemblePairs.py"
BUILD_CONSENSUS_PY="BuildConsensus.py"
CLUSTER_SETS_PY="ClusterSets.py"
COLLAPSE_SEQ_PY="CollapseSeq.py"
CONVERT_HEADERS_PY="ConvertHeaders.py"
FILTER_SEQ_PY="FilterSeq.py"
MASK_PRIMERS_PY="MaskPrimers.py"
PAIR_SEQ_PY="PairSeq.py"
PARSE_HEADERS_PY="ParseHeaders.py"
PARSE_LOG_PY="ParseLog.py"
SPLIT_SEQ_PY="SplitSeq.py"
VDJ_PIPE=./bin/vdj_pipe
PYTHON3="python3"

# bring in common functions
source presto_common.sh

# Start
printf "START at $(date)\n\n"

gather_secondary_inputs
print_parameters
print_versions
run_presto_workflow

# End
printf "DONE at $(date)\n\n"

# remove binaries before archiving 
rm -rf bin lib

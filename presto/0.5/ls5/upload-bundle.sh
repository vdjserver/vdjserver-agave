#
SYSTEM=ls5
VER=0.5

#
# Presto is special in that it uses the production
# area instead of bundling the executables. This is because
# presto and possibly other python programs and libraries
# have difficulty when being relocated. So right now only
# vdj_pipe is put in the bundle
#

# Copy all of the object files to the bundle directory
# and create a binaries.tgz
#
# For example:
# cd bundle
# mkdir bin
# cp $WORK/vdj_pipe/v0.1.7/out/apps/gcc-4.9.3/release/link-static/threading-multi/vdj_pipe bin
#
# Install pRESTO locally (python 3):
# module unload python
# module load gcc python3
# cd presto
# export PYTHONPATH=../lib/python3.5/site-packages:$PYTHONPATH
# python3 setup.py install --prefix=..
#
# tar zcvf binaries.tgz bin lib

# delete old working area in agave
files-delete /apps/presto/$VER/$SYSTEM

# upload files
files-mkdir -N $VER /apps/presto
files-mkdir -N $SYSTEM /apps/presto/$VER
files-mkdir -N test /apps/presto/$VER/$SYSTEM

files-upload -F bundle/binaries.tgz /apps/presto/$VER/$SYSTEM
files-upload -F presto.sh /apps/presto/$VER/$SYSTEM
files-upload -F presto.json /apps/presto/$VER/$SYSTEM
files-upload -F ../common/presto_common.sh /apps/presto/$VER/$SYSTEM
files-upload -F ../../../common/process_metadata.py /apps/presto/$VER/$SYSTEM
files-upload -F ../common/statistics-template.json /apps/presto/$VER/$SYSTEM
files-upload -F ../common/statistics.py /apps/presto/$VER/$SYSTEM

# num files plus . and test
files-list -L /apps/presto/$VER/$SYSTEM
if [ "$(files-list -L /apps/presto/$VER/$SYSTEM | wc -l)" -ne 9 ]; then
    echo "ERROR: Wrong number of file entries in app directory."
fi

files-upload -F test/test.sh /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-cli.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_40000.fastq /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-multiple.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_2000.fastq /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-primers.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_400000.fastq.gz /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/R1_barcodes.fasta /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/R2_barcodes.fasta /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-roche-presto.json /apps/presto/$VER/$SYSTEM/test
files-upload -F test/test-names1.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR765688.fastq /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR765688.fq.gz /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR765688_MIDs.txt /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRX190717_CPrimers.txt /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRX190717_VPrimers.txt /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-grieff-presto.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/ERR346600_1.fastq /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/ERR346600_2.fastq /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Greiff2014_VPrimers.fasta /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Greiff2014_CPrimers.fasta /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-stern-presto.json /apps/presto/$VER/$SYSTEM/test
files-upload -F test/test-names2.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR1383456_1.fastq /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR1383456_2.fastq /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR1383456_1.fq.gz /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/SRR1383456_2.fq.gz /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Stern2014_CPrimers.fasta /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/Stern2014_VPrimers.fasta /apps/presto/$VER/$SYSTEM/test

files-upload -F test/test-secondary.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/test/study_metadata.json /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/testJob1.zip /apps/presto/$VER/$SYSTEM/test
files-upload -F ../common/testJob2.zip /apps/presto/$VER/$SYSTEM/test

# num files plus .
files-list -L /apps/presto/$VER/$SYSTEM/test
if [ "$(files-list -L /apps/presto/$VER/$SYSTEM/test | wc -l)" -ne 34 ]; then
    echo "ERROR: Wrong number of file entries in test directory."
fi

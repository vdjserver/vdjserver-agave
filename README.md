VDJServer-Agave
===============
Scripts and JSON files for deploying VDJServer apps in Agave.

Production
==========

* systems: JSON definitions for execution/storage systems
* vdjpipe: JSON definitions for VDJPipe application
* igblast: JSON definitions for IgBlast application
* presto:  JSON definitions for pRESTO application
* repcalc:  JSON definitions for RepCalc application
* common: Common scripts used by all applications

Development
===========
These are for setting up personal development versions for testing.

* dev-system: JSON definitions for execution/storage systems

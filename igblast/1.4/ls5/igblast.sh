# VDJ Server igBlast Agave wrapper script
# Lonestar 5

# Configuration settings

VDJML_VERSION=1.0.0
# Path is slightly different between ls5 and stampede
IGDATA="$WORK/../common/igblast-db/db"

# automatic parallelization of large files
READS_PER_FILE=10000

# Agave input
ProjectDirectory="${ProjectDirectory}"
JobFiles="${JobFiles}"
query="${query}"
# Agave parameters
SecondaryInputsFlag=${SecondaryInputsFlag}
QueryFilesMetadata="${QueryFilesMetadata}"
species=${species}
ig_seqtype=${ig_seqtype}
domain_system="${domain_system}"

# Agave info
AGAVE_JOB_ID=${AGAVE_JOB_ID}
AGAVE_JOB_NAME=${AGAVE_JOB_NAME}
AGAVE_LOG_NAME=${AGAVE_JOB_NAME}-${AGAVE_JOB_ID}

# ----------------------------------------------------------------------------
tar zxf binaries.tgz  # Unpack local executables
tar zxf launcher-3.0.1.tar.gz
chmod +x fastq2fasta.py splitfasta.pl

# load modules
module load python
#module load launcher

IGBLASTN_EXE=igblastn
IGBLAST_PARSE_PY="python lib/python2.7/site-packages/vdjml/igblast_parse.py"
PYTHON=python
REPSUM_PY=repsum

export PATH="$PWD/bin:${PATH}"
export PYTHONPATH=$PWD/lib/python2.7/site-packages:$PYTHONPATH

# bring in common functions
source igblast_common.sh

# Start
printf "START at $(date)\n\n"

gather_secondary_inputs
print_parameters
print_versions
run_igblast_workflow

# End
printf "DONE at $(date)\n\n"

# remove executables and libraries before archiving 
rm -rf bin lib include launcher-3.0.1

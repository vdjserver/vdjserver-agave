#
SYSTEM=stampede
VER=1.4

# Copy all of the object files to the bundle directory
# and create a binaries.tgz
#
# For example:
# cd bundle
# cp -rf $WORK/stampede/igblast/local/* .
# cp $WORK/stampede/vdjml/develop/out/VDJMLpy-*.tar.gz .
# cp $WORK/common/launcher/launcher-3.0.1.tar.gz .
#
# Install repsum locally:
# cd repertoire-summarization
# export PYTHONPATH=../lib/python2.7/site-packages:$PYTHONPATH
# python setup.py install --prefix=..
#
# Install VDJML locally:
# cd VDJMLpy-*
# export PYTHONPATH=../lib/python2.7/site-packages:$PYTHONPATH
# python setup.py install --prefix=..
#
# install libyaml locally (used by AIRR-formats):
# cd yaml-0.1.7
# ./configure --prefix=/absolute path to bundle dir
# make
# make install
#
# Install AIRR-formats locally (used by Change-O, python 3):
# module unload python
# module load gcc python3
# cd airr-formats/lang/python
# export PYTHONPATH=../../../lib/python3.5/site-packages:$PYTHONPATH
# python3 setup.py install --prefix=../../..
#
# Install Change-O locally (python 3):
# module unload python
# module load gcc python3
# cd changeo
# export PYTHONPATH=../lib/python3.5/site-packages:$PYTHONPATH
# python3 setup.py install --prefix=..
#
# tar zcvf binaries.tgz bin lib include

# delete old working area in agave
files-delete /apps/igblast/$VER/$SYSTEM

# upload files
files-mkdir -N $VER /apps/igblast
files-mkdir -N $SYSTEM /apps/igblast/$VER
files-mkdir -N test /apps/igblast/$VER/$SYSTEM
files-upload -F bundle/binaries.tgz /apps/igblast/$VER/$SYSTEM
files-upload -F bundle/launcher-3.0.1.tar.gz /apps/igblast/$VER/$SYSTEM
files-upload -F ../common/fastq2fasta.py /apps/igblast/$VER/$SYSTEM
files-upload -F ../common/splitfasta.pl /apps/igblast/$VER/$SYSTEM
files-upload -F ../common/igblast_common.sh /apps/igblast/$VER/$SYSTEM
files-upload -F ../common/do_makedb.sh /apps/igblast/$VER/$SYSTEM
files-upload -F ../../../common/process_metadata.py /apps/igblast/$VER/$SYSTEM
files-upload -F igblast.sh /apps/igblast/$VER/$SYSTEM

files-upload -F test/test.sh /apps/igblast/$VER/$SYSTEM/test
files-upload -F test/test-cli.json /apps/igblast/$VER/$SYSTEM/test
files-upload -F test/test-mixed-cli.json /apps/igblast/$VER/$SYSTEM/test
files-upload -F test/test-big-cli.json /apps/igblast/$VER/$SYSTEM/test
files-upload -F test/test-secondary.json /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/01.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/02.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/03.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/funky_ids.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/with_dupcount.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/special_seqs.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_2000.fastq /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_40000.fastq /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/test_40000.fastq /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/Merged_400000.fastq.gz /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/Test_2000.fasta /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/test/study_metadata.json /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/testJob1.zip /apps/igblast/$VER/$SYSTEM/test
files-upload -F ../common/testJob2.zip /apps/igblast/$VER/$SYSTEM/test

files-list -L /apps/igblast/$VER/$SYSTEM
if [ "$(files-list -L /apps/igblast/$VER/$SYSTEM | wc -l)" -ne 10 ]; then
    echo "ERROR: Wrong number of file entries in app directory."
fi

files-list -L /apps/igblast/$VER/$SYSTEM/test

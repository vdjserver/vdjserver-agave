# VDJ Server igBlast Agave wrapper script
# Lonestar 5

# Configuration settings

VDJML_VERSION=1.0.0
# Path is slightly different between ls5 and stampede
IGDATA="$WORK/../common/igblast-db/db.2018.03.15"

# automatic parallelization of large files
READS_PER_FILE=10000

# Agave input
ProjectDirectory="${ProjectDirectory}"
JobFiles="${JobFiles}"
query="${query}"
# Agave parameters
SecondaryInputsFlag=${SecondaryInputsFlag}
QueryFilesMetadata="${QueryFilesMetadata}"
species=${species}
ig_seqtype=${ig_seqtype}
domain_system="${domain_system}"

# Agave info
AGAVE_JOB_ID=${AGAVE_JOB_ID}
AGAVE_JOB_NAME=${AGAVE_JOB_NAME}
AGAVE_LOG_NAME=${AGAVE_JOB_NAME}-${AGAVE_JOB_ID}

# ----------------------------------------------------------------------------
tar zxf binaries.tgz  # Unpack local executables
tar zxf launcher-2018_02_22.tar.gz
chmod +x fastq2fasta.py splitfasta.pl

# load modules
module load python

IGBLASTN_EXE="igblastn -num_threads 1"
IGBLAST_PARSE_PY="python lib/python2.7/site-packages/vdjml/igblast_parse.py"
PYTHON=python
REPSUM_PY=repsum

export PATH="$PWD/bin:${PATH}"
export PYTHONPATH=$PWD/lib/python2.7/site-packages:$PYTHONPATH
export CHANGEO_PYTHON=$PWD/lib/python3.5/site-packages

# ----------------------------------------------------------------------------
# Launcher to use multicores on node
export LAUNCHER_DIR=$PWD/launcher
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins
export LAUNCHER_WORKDIR=$PWD
export LAUNCHER_LOW_PPN=4
export LAUNCHER_MID_PPN=12
export LAUNCHER_MAX_PPN=20
export LAUNCHER_PPN=1
export LAUNCHER_JOB_FILE=joblist
export LAUNCHER_SCHED=interleaved
export LAUNCHER_BIND=0

# bring in common functions
source igblast_common.sh

# Start
printf "START at $(date)\n\n"

gather_secondary_inputs
print_parameters
print_versions
run_igblast_workflow

# End
printf "DONE at $(date)\n\n"

# remove executables and libraries before archiving 
rm -rf bin lib include launcher

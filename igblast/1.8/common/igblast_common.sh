#
# VDJServer IgBlast common functions
#
# This script relies upon global variables
# source igblast_common.sh
#
# Author: Scott Christley
# Date: Sep 1, 2016
# 

# required global variables:
# IGBLASTN_EXE
# IGBLAST_PARSE_PY
# REPSUM_PY
# IGDATA
# PYTHON
# AGAVE_JOB_ID
# AGAVE_LOG_NAME
# and...
# The agave app input and parameters

APP_NAME=igBlast

# IgBlast germline database and extra files
IGBLAST_VERSION=1.8.0
IGBLAST_URI=http://www.ncbi.nlm.nih.gov/projects/igblast/
VDJ_DB_VERSION=03_15_2018
VDJ_DB_URI=http://wiki.vdjserver.org/vdjserver/index.php/VDJServer_IgBlast_Database
export IGDATA
export VDJ_DB_ROOT="$IGDATA/$VDJ_DB_VERSION/"

# ----------------------------------------------------------------------------
function expandfile () {
    fileBasename="${1%.*}" # file.txt.gz -> file.txt
    fileExtension="${1##*.}" # file.txt.gz -> gz

    if [ ! -f $1 ]; then
        echo "Could not find query input file $1" 1>&2
        exit 1
    fi

    if [ "$fileExtension" == "gz" ]; then
        gunzip $1
        export file=$fileBasename
        # don't archive the intermediate file
    elif [ "$fileExtension" == "bz2" ]; then
        bunzip2 $1
        export file=$fileBasename
    elif [ "$fileExtension" == "zip" ]; then
        unzip $1
        export file=$fileBasename
    else
        export file=$1
    fi
}

function noArchive() {
    echo $1 >> .agave.archive
}

# ----------------------------------------------------------------------------
# Process workflow metadata
function initProcessMetadata() {
    $PYTHON ./process_metadata.py --init $APP_NAME $AGAVE_JOB_ID process_metadata.json
    # collect all output files
    mkdir ${AGAVE_JOB_ID}
    noArchive ${AGAVE_JOB_ID}
    ARCHIVE_FILE_LIST=""
}

function addLogFile() {
    $PYTHON ./process_metadata.py --entry log "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
}

function addConfigFile() {
    $PYTHON ./process_metadata.py --entry config "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
}

function addOutputFile() {
    $PYTHON ./process_metadata.py --entry output "$1" "$2" "$3" "$4" "$5" "$6" "$7" process_metadata.json
    # add file to list to be archived
    ARCHIVE_FILE_LIST="${ARCHIVE_FILE_LIST} $4"
}

function addGroup() {
    $PYTHON ./process_metadata.py --group "$1" "$2" process_metadata.json
}

function addCalculation() {
    $PYTHON ./process_metadata.py --calc $1 process_metadata.json
}

# ----------------------------------------------------------------------------
# IgBlast workflow

function gather_secondary_inputs() {
    # Gather secondary input files
    # This is used to get around Agave size limits for job inputs and parameters
    if [[ $SecondaryInputsFlag -eq 1 ]]; then
	echo "Gathering secondary inputs"
	moreFiles=$(${PYTHON} ./process_metadata.py --getSecondaryInput "${ProjectDirectory}/" QueryFilesMetadata study_metadata.json)
	query="${query} ${moreFiles}"
	moreFiles=$(${PYTHON} ./process_metadata.py --getSecondaryEntry QueryFilesMetadata study_metadata.json)
	QueryFilesMetadata="${QueryFilesMetadata} ${moreFiles}"
    fi
}

function print_versions() {
    echo "VERSIONS:"
    echo "  $($IGBLASTN_EXE -version 2>&1)"
    #echo "  $(airr-tools --version 2>&1)"
    echo -e "\nSTART at $(date)"
}

function print_parameters() {
    echo "Input files:"
    echo "ProjectDirectory=${ProjectDirectory}"
    echo "JobFiles=${JobFiles}"
    echo "query=$query"
    echo ""
    echo "Application parameters:"
    echo "SecondaryInputsFlag=${SecondaryInputsFlag}"
    echo "QueryFilesMetadata=$QueryFilesMetadata"
    echo "species=$species"
    echo "ig_seqtype=$ig_seqtype"
    echo "domain_system=$domain_system"
}

function run_igblast_workflow() {
    initProcessMetadata
    addLogFile $APP_NAME log stdout "${AGAVE_LOG_NAME}.out" "Job Output Log" "log" null
    addLogFile $APP_NAME log stderr "${AGAVE_LOG_NAME}.err" "Job Error Log" "log" null
    addLogFile $APP_NAME log agave_log .agave.log "Agave Output Log" "log" null
    addCalculation vdj_alignment
    addCalculation parse_igblast
    addCalculation repertoire_summarization

    # Exclude input files from archive
    noArchive "${ProjectDirectory}"
    for file in $JobFiles; do
	if [ -f $file ]; then
	    unzip $file
	    noArchive $file
	    noArchive "${file%.*}"
	fi
    done

    # launcher job file
    if [ -f joblist ]; then
	echo "Warning: removing file 'joblist'.  That filename is reserved." 1>&2
	rm joblist
	touch joblist
    fi
    echo "joblist" >> .agave.archive

    filelist=()
    count=0
    for file in $query; do
	# unique group name to put in metadata
	group="group${count}"
	addGroup $group file

	fileOutname="${file##*/}" # test/file -> file
	addOutputFile $group $APP_NAME assignment_sequence "$file" "Input Sequences ($fileOutname)" "read" null

	expandfile $file
	echo $file >> .agave.archive
	fileExtension="${file##*.}" # file.fastq -> fastq
	fileBasename="${file%.*}" # file.fastq -> file

	if [[ "$fileExtension" == "fastq" ]] || [[ "$fileExtension" == "fq" ]]; then
            ./fastq2fasta.py -i $file -o $fileBasename.fasta
            file="$fileBasename.fasta"
	    echo $file >> .agave.archive
	fi

        # save expanded filenames for later merging
	filelist[${#filelist[@]}]=$file

	if [ "$(grep '>' $file | wc -l)" -gt $READS_PER_FILE ]; then
            splitfasta.pl -f $file -r $READS_PER_FILE -o . -s ${fileBasename}_p
            smallFiles="$(ls ${fileBasename}_p*.fasta)"
	else
            smallFiles="$file"
	fi

	for smallFile in $smallFiles; do
	    echo $smallFile >> .agave.archive
	    echo ${smallFile}.igblast.out >> .agave.archive

            # These come from Agave, but I need to assign them inside the loop.
            organism=${species}
            seqType=${ig_seqtype}
	    QUERY_ARGS=""
            ARGS=""
            IPARGS=""
            RSARGS=""
	    MDARGS=""
            if [ -f $smallFile ]; then 
		QUERY_ARGS="-query $smallFile" 
		IPARGS="$IPARGS $PWD/${smallFile}.igblast.out" 
		IPARGS="$IPARGS $PWD/${smallFile}.igblast.out.vdjml"
		RSARGS="$RSARGS $PWD/${smallFile}.igblast.out.vdjml"
		RSARGS="$RSARGS $PWD/${smallFile}.igblast.out.${domain_system}.rc_out.tsv"
		RSARGS="$RSARGS $VDJ_DB_ROOT" 
		RSARGS="$RSARGS $smallFile"
		MDARGS="$MDARGS $smallFile"
		MDARGS="$MDARGS $PWD/${smallFile}.igblast.out"
            fi
            if [ -n $seqType ]; then 
		ARGS="$ARGS -ig_seqtype $seqType"
		if [ "$seqType" == "TCR" ]; then seqType="TR"; fi  
		if [ "$seqType" == "Ig" ]; then seqType="IG"; fi  
		MDARGS="$MDARGS $seqType"
            fi
            if [ -n $organism ]; then 
		ARGS="$ARGS -organism $organism"
		ARGS="$ARGS -auxiliary_data $IGDATA/optional_file/${organism}_gl.aux"
		if [ "$organism" == "mouse" ]; then organism="Mus_musculus"; fi
		ARGS="$ARGS -germline_db_V $VDJ_DB_ROOT/$organism/ReferenceDirectorySet/${organism}_${seqType}_V.fna"
		ARGS="$ARGS -germline_db_D $VDJ_DB_ROOT/$organism/ReferenceDirectorySet/${organism}_${seqType}_D.fna"
		ARGS="$ARGS -germline_db_J $VDJ_DB_ROOT/$organism/ReferenceDirectorySet/${organism}_${seqType}_J.fna"
		RSARGS="$RSARGS $organism" 
		MDARGS="$MDARGS $organism"
            fi
            if [ -n $domain_system ]; then ARGS="$ARGS -domain_system $domain_system"; fi

            IPARGS="$IPARGS -db_name_igblast ${organism}_${seqType}"
            IPARGS="$IPARGS -db_ver $VDJ_DB_VERSION"
            IPARGS="$IPARGS -db_species_vdjml $organism"
            IPARGS="$IPARGS -db_uri $VDJ_DB_URI"
            IPARGS="$IPARGS -igblast_version $IGBLAST_VERSION"
            IPARGS="$IPARGS -igblast_uri $IGBLAST_URI"
            # IPARGS="$IPARGS -igblast_runid VDJServer-$JOBID" # disabled for VDJML issue #20
	    IGBLAST_PARAMS="$ARGS"

            ARGS="$QUERY_ARGS $ARGS -outfmt "
            OUTFMT="7 qseqid qgi qacc qaccver qlen sseqid sallseqid sgi sallgi sacc saccver sallacc slen qstart qend sstart send qseq sseq evalue bitscore score length pident nident mismatch positive gapopen gaps ppos frames qframe sframe btop"
            
            #echo "export IGDATA=\"$IGDATA\" && $IGBLASTN_EXE $ARGS \"$OUTFMT\" | $IGBLAST_PARSE_PY $IPARGS -igblast_params \"$IGBLAST_PARAMS\" && $REPSUM_PY $RSARGS" >> joblist
            echo "export IGDATA=\"$IGDATA\" && export VDJ_DB_ROOT=\"$VDJ_DB_ROOT\" && $IGBLASTN_EXE $ARGS \"$OUTFMT\" > ${smallFile}.igblast.out && $IGBLAST_PARSE_PY $IPARGS -igblast_params \"$IGBLAST_PARAMS\" && $REPSUM_PY $RSARGS && bash ./do_makedb.sh $MDARGS" >> joblist
	done

	count=$(( $count + 1 ))
    done

    # check number of jobs to be run
    export LAUNCHER_PPN=$LAUNCHER_MAX_PPN
    numJobs=$(cat joblist | wc -l)
    if [ $numJobs -lt $LAUNCHER_PPN ]; then
	export LAUNCHER_PPN=$numJobs
    fi

    echo "Starting igblast on $(date)"
    
    $LAUNCHER_DIR/paramrun

    # ----------------------------------------------------------------------------
    # and now to knit smallFiles back together
    seqMetadata=($QueryFilesMetadata)
    count=0
    for file in ${filelist[@]}; do
	fileBasename="${file%.*}" # test/file.fasta -> test/file
	fileOutname="${fileBasename##*/}" # test/file -> file
	checkfiles=(`ls -1 ${fileBasename}_p*vdjml 2>/dev/null`)

	if [ ${#checkfiles[@]} -ne 0 ]; then
 	    # merge files
	    vdjml_merge.py ${fileBasename}_p*vdjml > ${fileOutname}.igblast.out.vdjml
	    igblast_tsv_merge.py ${fileBasename}_p*.igblast.out.${domain_system}.rc_out.tsv > ${fileOutname}.igblast.out.${domain_system}.rc_out.tsv
	    igblast_tsv_merge.py ${fileBasename}_p*.igblast.airr.tsv > ${fileOutname}.igblast.airr.tsv
	    igblast_tsv_merge.py ${fileBasename}_p*.igblast_db-pass.tsv > ${fileOutname}.igblast_db-pass.tsv
	    # remove intermediate files
	    rm -f ${fileBasename}_p*.igblast.out.${domain_system}.rc_out.tsv
	    rm -f ${fileBasename}_p*vdjml
	    rm -f ${fileBasename}_p*.igblast.airr.tsv
	    rm -f ${fileBasename}_p*.igblast_db-pass.tsv
	else
	    # no merging so rename to remove extension
	    mv ${file}.igblast.out.vdjml ${fileOutname}.igblast.out.vdjml
	    mv ${file}.igblast.out.${domain_system}.rc_out.tsv ${fileOutname}.igblast.out.${domain_system}.rc_out.tsv
	    mv ${file}.igblast.airr.tsv ${fileOutname}.igblast.airr.tsv
	    mv ${file}.igblast_db-pass.tsv ${fileOutname}.igblast_db-pass.tsv
	fi

	# they will be compressed below
	group="group${count}"
	mfile=${seqMetadata[count]}
	addOutputFile $group $APP_NAME vdjml ${fileOutname}.igblast.out.vdjml.zip "${fileOutname} VDJML" "vdjml" $mfile
	addOutputFile $group $APP_NAME summary ${fileOutname}.igblast.out.${domain_system}.rc_out.tsv.zip "${fileOutname} RepSum TSV" "tsv" $mfile
	addOutputFile $group $APP_NAME airr ${fileOutname}.igblast.airr.tsv.zip "${fileOutname} AIRR TSV" "tsv" $mfile
	addOutputFile $group $APP_NAME changeo ${fileOutname}.igblast_db-pass.tsv.zip "${fileOutname} Change-O TSV" "tsv" $mfile

	count=$(( $count + 1 ))
    done

    # ----------------------------------------------------------------------------
    # compress the vdjml and tsv files
    echo Compressing output files
    for file in ${filelist[@]}; do
	fileBasename="${file%.*}"
	fileOutname="${fileBasename##*/}"
	zip ${fileOutname}.igblast.out.vdjml.zip ${fileOutname}.igblast.out.vdjml
	rm ${fileOutname}.igblast.out.vdjml
	zip ${fileOutname}.igblast.out.${domain_system}.rc_out.tsv.zip ${fileOutname}.igblast.out.${domain_system}.rc_out.tsv
	rm ${fileOutname}.igblast.out.${domain_system}.rc_out.tsv
	#zip ${fileOutname}.igblast.airr.tsv.zip ${fileOutname}.igblast.airr.tsv ${fileOutname}.igblast.airr.tsv.meta.json
	# TODO: currently disable metadata file, need to write merge program
	zip ${fileOutname}.igblast.airr.tsv.zip ${fileOutname}.igblast.airr.tsv
	rm ${fileOutname}.igblast.airr.tsv
	#rm ${fileOutname}.igblast.airr.tsv.meta.json
	zip ${fileOutname}.igblast_db-pass.tsv.zip ${fileOutname}.igblast_db-pass.tsv
	rm ${fileOutname}.igblast_db-pass.tsv
    done

    # zip archive of all output files
    for file in $ARCHIVE_FILE_LIST; do
	if [ -f $file ]; then
	    cp -f $file ${AGAVE_JOB_ID}
	fi
    done
    zip ${AGAVE_JOB_ID}.zip ${AGAVE_JOB_ID}/*
    addLogFile $APP_NAME log output_archive ${AGAVE_JOB_ID}.zip "Archive of Output Files" "zip" null
}

# Agave IgBlast Application for VDJServer

The general naming of the Agave IgBlast application is
`igblast-SYSTEM-VER` where `SYSTEM` is the short name of the execution
system and `VER` is the `MAJOR.MINOR` version number of IgBlast. We
use just the `MAJOR.MINOR` version numbers for the Agave
application. This allows for incorporating bug fix releases without
having to generate a new version.

* igblast-ls5-1.4: IgBlast 1.4.0 for Lonestar 5.

* igblast-stampede-1.4: IgBlast 1.4.0 for Stampede.

* igblast-stampede2-1.8: IgBlast 1.8.0 for Stampede2.

Note that almost all of the binaries for the application are bundled
together into the Agave application, making each application
self-contained with minimal external dependencies. This also acts as
an effective archival and versioning mechanism. There are exceptions
however as some applications assets are quite large causing
inefficiencies if bundled with the main application. For IgBlast,
these exceptions include:

* Germline database: The germline database and associated BLAST
  database files are stored in the common `$WORK` area
  `$WORK/../common/igblast-db/db`. The `IGDATA` environment variable
  is set to point to that directory.

## Directory structure and files

Directories are split by IgBlast version. Under each are a set of
directories for the different execution systems:

* common: scripts and data common for all execution systems. Most of
  the main processing workflow is written to be system independent and
  resides in `igblast_common.sh`. Data for tests are stored here under
  the `test` directory.

* ls5: IgBlast for ls5.tacc.utexas.edu

* stampede: IgBlast for stampede.tacc.utexas.edu (DEPRECATED)
  Stampede2 has supersceded Stampede.

* stampede2: IgBlast for stampede2.tacc.utexas.edu.

Each execution system directory has a set of files that is similar
contains customizations specific to the execution system. The
customizations handle difference such as machine/queue names, system
modules, directory paths, environment variables, parallel processing
settings, etc.

* upload-bundle.sh: Simple shell script to use Agave CLI command to
  upload all of the files for the application into the Agave storage
  system. Typically defining `SYSTEM` and `VER` at the top of the
  script is sufficient.

* igblast.sh: The job submission template for the Agave
  application. When Agave runs a job, it takes this template script
  and modifies it into a submission script that is then submitted to
  the queue on the execution system. This script has been written to
  move as much system independent code into the common script, which
  are called as bash functions. The script setups the execution
  environment by extracting binaries, setting system modules and
  environment variables.

* igblast.json: The JSON definition for the Agave IgBlast
  application. This definition should be identical for each execution
  system except for a few parameters. The `inputs` and `parameters`
  should be identical for all execution systems.

* test: A set of JSON job scripts for manually testing the Agave
  application. Archive is off by default, so that the scratch
  directory can be reviewed and debugged. All jobs should run
  successfully and the job error file in the scratch directory should
  be checked to insure no errors occurred. All job data is stored in
  the `common` directory but is uploaded separately into the specific
  application's directory tree within Agave.

* bundle: This is a temporary work area for combining all the binaries
  together into a `binaries.tgz` file that is upload as part of the
  application. This directory is excluded from the git repository.

## Deploying and Publishing Agave IgBlast application

If you have a new IgBlast version, or a new execution system, then you
will be starting from scratch. Generally, you should copy all the
files from another execution system as a starting point, then make the
necessary modifications to them. The complete steps include:

1. Logon execution system as vdj account.

2. Compiling IgBlast

3. Modify igblast.json

4. Modify igblast.sh

5. Modify test scripts

6. Create binary bundle

7. Upload files to Agave

8. Create Agave application

9. Submit test jobs

10. Publish application

If you are modifying an existing version versus starting from scratch,
then generally steps 6, 7 and 9, are all that is performed. However,
if the JSON definition was modified, then step 8 needs to be performed
to update the Agave app definition.

### Logon execution system as vdj account

You want to be on the same execution system for the application you
are deploying. You cannot login directly into the vdj account, instead
you need to login with your user account, then `su` to the vdj account.

```
$ ssh schristl@stampede2.tacc.utexas.edu
Password: 
TACC Token Code:

$ su - vdj
Password: 

$ source vdjserver.env
$ auth-tokens-refresh
```

Use the vdj account password for the `su` command. The `source`
command sets up some environment like adding the Agave CLI tools to
the path. If `auth-tokens-refresh` returns an error, then the token
may have expired, so use `auth-tokens-create -u vdj` to create a new
one. You will need the Agave password for the vdj account. If you need
to specify the API client key and secret, be sure to use either the
`vdj_dev` or `vdj_staging` client so that your tokens do not conflict
with the VDJServer production system using the `vdj` client.

You can verify that the Agave CLI tools are working by trying a few commands:

```
$ files-list /
$ jobs-list -l 10
```

### Compiling IgBlast

NCBI does provide pre-built binaries for IgBlast but they may not work
or be efficiently built for the specific supercomputer
system. Compilation is done in `$WORK/igblast`. These are primarily
notes to help document and may not be a complete set of
instructions. On a new system, you may need to figure out the proper
way to compile.

#### IgBlast 1.8.0 on Stampede2

I tried compiling with GCC but it eventually failed complaining about
GNUTLS, which didn't have a clear solution. I then tried with the
Intel compiler and that seemed to work.

```
$ cd $WORK/igblast
$ cd 1.8.0
$ mkdir local
$ tar zxvf ncbi-igblast-1.8.0-src.tar.gz
$ cd ncbi-igblast-1.8.0-src/c++
$ module purge
$ module load TACC
$ icpc -V
Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 17.0.4.196 Build 20170411
Copyright (C) 1985-2017 Intel Corporation.  All rights reserved.

$ icc -V
Intel(R) C Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 17.0.4.196 Build 20170411
Copyright (C) 1985-2017 Intel Corporation.  All rights reserved.

$ ./configure --prefix=$WORK/igblast/1.8.0/local CC=icc CXX=icpc
$ make
$ make install
```

The binaries should now reside in `$WORK/igblast/1.8.0/local/bin` and
can be copied to the application bundle.

### Modify igblast.json

Customize for the execution system:

* name: igblast-SYSTEM where `SYSTEM` is the short name for the
  execution system (ls5, stampede2).

* version: `MAJOR.MINOR` version of IgBlast. This will be added to the
  end of `name` to create the unique Agave app id.

* shortDescription, longDescription: basic description

* deploymentPath: `/apps/igblast/VER/SYSTEM` where `VER` and `SYSTEM`
  are the values used in all the other places,
  e.g. `/apps/igblast/1.8/stampede2`.

* executionSystem: Agave id for execution system,
  e.g. `stampede2.tacc.utexas.edu`

The execution system definition defines much of the technical details
of the system like the scheduling system, queue names, and so on but
some of these defaults can be overridden for specifications
applications. You should review these if they are defined:
`defaultQueue`, `defaultNodeCount`, `defaultProcessorsPerNode`,
`defaultRequestedTime`. There may be different attributes for
different types of execution systems.

### Modify igblast.sh

It's important that this script defines environment variables for the
`inputs` and `parameters` defined in the app JSON because this is the
only way to get those values. Agave uses this file as a template, and
search/replace those variables in the script with their values, as it
write the job submission script.

This script should load appropriate system modules, unarchive the
binary bundle, setup environment variables, and then call the main
system independent functions to run the workflow. Programs and the
commands to execute them may vary from system to system, so most apps
define environment variable with that information,
e.g. `IGBLAST_PARSE_PY` for the script that parses IgBlast output for
VDJML.

### Modify test scripts

Each test script needs `appId` and `executionSystem` to be
defined. Also all directory paths in the `inputs` and `parameters`
need to point to the proper `VER` and `SYSTEM`.

### Create binary bundle

The Agave IgBlast applications actually contains more programs than
just IgBlast. It also contains VDJML, RepSum, AIRR, pRESTO and
ChangeO. We use the `bundle` directory to gather all of these tools
together. They are installed in a local, relative directory so that
the whole bundle can be copied and used elsewhere, like the scratch
directory.

The `upload-bundle.sh` script contains notes for the command to be
used in comments at the beginning of the script. These notes are
approximate as I've not tried to setup a complete automated system, as
you typically don't need to do everything when doing an update. For
example, if you are updating to a newer ChangeO, you generally only
need to re-install ChangeO and can leave the other tools as is. The
last command is to `tar` the `bin`, `lib` and possibly other
directories into `binaries.tgz` which will get uploaded.

### Upload files to Agave

All the files for the application bundle need to be uploaded before
the Agave application can be defined. The `upload-bundle.sh` should be
modified with the appropriate `SYSTEM` and `VER` values, as
adding/removing files to be uploaded. The scripts uses the Agave CLI
so you need an active token for the vdj account.

This script also contains notes for the commands used to create the
binary bundle.

### Create Agave application

The application can be created and updated with the same command.

```
$ apps-addupdate -F igblast.json
Successfully added app igblast-stampede2-1.8
```

Once the application is defined in Agave, the `apps-addupdate` command
only needs to be re-run when the JSON definition changes. For most
changes, you can just re-upload the binary bundle then run a test job.

### Submit test jobs

All tests should submit and run successfully. Verify that they
actually ran the application you expected. Review the log files in the
scratch directory for any errors. If you find errors, you will need to
go back and repeat appropriate steps. It is also useful for debugging
to modify the submission script in the scratch directory, then use
`idev` to interactively run and debug the app.

```
$ jobs-submit -F test-cli.json
```

### Publish application

The application resides as a development application. That is, when a
job is run, Agave copies the `deploymentPath` directory tree to the
scratch directory. Thus any changes to files in that `deploymentPath`
will change the application. For production, we want a fixed version
of the application that won't change. The `apps-publish` command does
this by making a zip archive of `deploymentPath` and putting it in the
`publicAppsDir` for the storage system, which is `/api/v2/apps` for
VDJServer. In the process it prefixes a revision number to create a
unique Agave app id.

```
$ apps-publish igblast-stampede2-1.8
Application published as igblast-stampede2-1.8u1
```
